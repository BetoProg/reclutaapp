import { Component } from '@angular/core';
import { AuthSerivce } from './auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'apprecluta';

  constructor(public authService:AuthSerivce){}
}
