import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'booleanos'
})
export class BooleanosPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    if(value===true){
      return 'Si';
    }else{
      return 'No';
    }
  }

}
