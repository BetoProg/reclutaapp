import { Component, OnInit, Output,EventEmitter } from '@angular/core';
import { AuthSerivce } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @Output() sidenavToggle = new EventEmitter<void>();
  constructor(public authService:AuthSerivce) { }

  ngOnInit() {
  }

  onsidenavToggle(){
    this.sidenavToggle.emit();
  }

  


}
