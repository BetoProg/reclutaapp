import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AuthSerivce } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit {
  @Output() sideNavClose = new EventEmitter<void>();
  constructor(public authService:AuthSerivce) { }

  ngOnInit() {
  }

  onClose(){
    this.sideNavClose.emit();
  }

}
