import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Colaborador } from '../models/Colaborador';
import { environment } from 'src/environments/environment';
import { map, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Puesto } from '../models/Puesto';
import Swal from 'sweetalert2';

@Injectable({
    providedIn:'root'
})
export class RegistroService {
    private url:string = environment.url;
    private header = new HttpHeaders({'Content-Type':'application/json'});

    constructor(private http:HttpClient){}

    insertarRegistroCol(colaborador:Colaborador){
        return this.http.post(`${this.url}/colaborador/nuevo`,colaborador,{headers:this.header})
        .pipe(
            map((res:any)=>{
                return res as any;
            }),
            catchError(err => {
                Swal.fire('Error en Inserción',err.error.mensaje,'error');
                return Observable.throw(err);
            })
        )
    }


    obtenerColaborador(id:number):Observable<Colaborador>{
        return this.http.get(`${this.url}/colaborador/obtener/${id}`)
        .pipe(
            map((res:any)=>{
                return res.colaborador as Colaborador;
            }),
            catchError(err => {
                Swal.fire('Error Obtener Datos',err.error.mensaje,'error');
                return Observable.throw(err);
            })
        );
    }

    obtenerColaboradores(){
        return this.http.get(`${this.url}/colaborador/todos`)
        .pipe(
            catchError(err => {
                Swal.fire('Error Obtener Datos',err.error.mensaje,'error');
                return Observable.throw(err);
            })
        );
    }

    actualizarColaboradorValidador(colaborador:Colaborador,id:number){
        return this.http.put(`${this.url}/colaborador/actualizar/${id}`,colaborador,{ headers:this.header })
        .pipe(
            catchError(err=>{
                Swal.fire('Error de servidor',err,'error');
                return Observable.throw(err);
            })
        );
    }

    actualizarColaboradorPuesto(puesto:Puesto,id:number){
        return this.http.put(`${this.url}/colaborador/actualizar/puesto/${id}`,puesto,{ headers:this.header })
        .pipe(
            catchError(err=>{
                Swal.fire('Error de servidor',err,'error');
                return Observable.throw(err);
            })
        );
    }
}