import { Component, OnInit, ViewChild } from '@angular/core';
import { RegistroService } from '../registro.service';
import { MatTableDataSource } from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import { ListarColaborador } from 'src/app/models/Colaborador';
import Swal from 'sweetalert2';
import { ValidacionService } from '../validacion.service';
import * as _moment from 'moment';
import { default as _rollupMoment } from 'moment';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';
const moment = _rollupMoment || _moment;

@Component({
  selector: 'app-lista-colaborador',
  templateUrl: './lista-colaborador.component.html',
  styleUrls: ['./lista-colaborador.component.css']
})
export class ListaColaboradoresComponent implements OnInit {
  loading=false;
  tituloBtn:string='Enviar Correo'
  listaColaboradores:ListarColaborador[] = [];
  displayedColumns: string[] = [
    'id',
    'nombre', 
    'apellido_paterno',
    'apellido_materno',
    'correo_electronico',
    'fecha_creacion',
    'opciones'
  ];

  dataSource = new MatTableDataSource<ListarColaborador>(this.listaColaboradores);
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  constructor(private regService:RegistroService,private validService:ValidacionService) { }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.loading = true;
    
    this.regService.obtenerColaboradores().subscribe((res:any)=>{
      this.listaColaboradores = res.colaboradores;
      this.dataSource.data = this.listaColaboradores;
      this.loading = false;
      
    },error=>{
      this.loading=false;
    });
  }

  setupFilter(column: string) {
    this.dataSource.filterPredicate = (d, filter: string) => {
      let momentObj = moment(d[column]);
      let fecha = momentObj.format('YYYY-MM-DD');
      const textToSearch = fecha && fecha.toLowerCase() || '';
      return textToSearch.indexOf(filter) !== -1;
    };
  }

  enviarCorreoColaborador(idcolaborador:number){
    this.loading = true;
    this.tituloBtn = 'Espere...';
    this.validService.reenviarCorreo(idcolaborador).subscribe((res:any)=>{
      if(res.ok){
        Swal.fire('Envio de Correo', res.mensaje,'success');
        this.loading = false;
        this.tituloBtn = 'Enviar Correo';
      }
    },error=>{
      this.loading = false;
    });
  }

  aplicarFiltro(value:any){
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  aplicarFiltroFecha(event:MatDatepickerInputEvent<Date>){
    let momentObj = moment(event.value);
    let fecha = momentObj.format('YYYY-MM-DD');
    this.dataSource.filter = fecha.trim().toLocaleLowerCase();
  }

}
