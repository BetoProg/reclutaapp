import { Injectable } from '@angular/core';
import { HttpHeaders,HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2';
import { environment } from 'src/environments/environment';
import { map, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Validacion } from '../models/Validaciones';

@Injectable({
    providedIn:'root'
})
export class ValidacionService {
    private url:string = environment.url;
    header:HttpHeaders = new HttpHeaders({'Content-Type':'application/json'});

    constructor(private http:HttpClient){}

    insertValidacion(validacion:Validacion){
        return this.http.post(`${this.url}/validacion/nuevo`,validacion,{headers:this.header})
        .pipe(
            map((res:any)=>{
                return res as any;
            }),
            catchError(err => {
                Swal.fire('Error en Validacion',err.error.mensaje,'error');
                return Observable.throw(err);
            })
        )
    }

    verificarValidaciones(idcolaborador:number){
        return this.http.get(`${this.url}/validacion/tienevalidacion/${idcolaborador}`)
        .pipe(
            catchError(err=>{
                Swal.fire('Error en obtener Datos','Ocurrio un error de servidor','error');
                return Observable.throw(err);
            })
        );
    }

    reenviarCorreo(idcolaborador:number){
        return this.http.get(`${this.url}/validacion/reenviarcorreo/${idcolaborador}`)
        .pipe(
            catchError(err=>{
                Swal.fire('Error en enviar correo',err.error.mensaje,'error');
                return Observable.throw(err);
            })
        )
    }

    actualizarEstatusValidacion(idcolaborador:number,estatus:boolean){
        return this.http.put(`${this.url}/validacion/editarestatus/${idcolaborador}/${estatus}`,{headers:this.header})
        .pipe(
            catchError(err=>{
                Swal.fire('Error en Servidor','No se pudo actualizar registro','error');
                return Observable.throw(err);
            })
        );
    }
    
    actualizarValidacion(validacion:Validacion, id:number){
        return this.http.put(`${this.url}/validacion/editar/${id}`,validacion,{ headers:this.header })
        .pipe(
            catchError(err=>{
                Swal.fire('Error en Servidor','No se pudo actualizar registro','error');
                return Observable.throw(err);
            })
        );
    }

    obtenerNumeroLetra(numero:number){
        return this.http.get(`${this.url}/validacion/numeroletras?numero=${numero}`);
    }

}