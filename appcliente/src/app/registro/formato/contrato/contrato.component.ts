import { Component, OnInit } from '@angular/core';
import { Colaborador } from 'src/app/models/Colaborador';
import { RegistroService } from '../../registro.service';
import { ActivatedRoute } from '@angular/router';
import { ValidacionService } from '../../validacion.service';

@Component({
  selector: 'app-contrato',
  templateUrl: './contrato.component.html',
  styleUrls: ['./contrato.component.css']
})
export class ContratoComponent implements OnInit {
  colaborador:Colaborador = new Colaborador();
  numeroString:string = '';

  opciones_radio = [
    {id:1, texto:'Quincenal'},
    {id:2, texto:'Semanal' }
  ]

  opcion_contrato:Opcion_Clausula;

  constructor(private regService:RegistroService,
    private validService:ValidacionService,
    private route:ActivatedRoute) { }

  ngOnInit() {
    this.cargarColaborador();
    this.opcion_contrato = CONTRATO_CLAUSULA.filter(x=>x.id == 1)[0];
  }

  cargarOpcionSemQuin(index){
    this.opcion_contrato = CONTRATO_CLAUSULA.filter(x=>x.id == index)[0];
  }

  cargarColaborador(){
    let idcolaborador = this.route.snapshot.paramMap.get('id');
  
    if(idcolaborador){
      this.regService.obtenerColaborador(Number(idcolaborador)).subscribe((res)=>{
        this.colaborador = res;
        this.cargarNumeroLetras(this.colaborador.salario_referencia);
      });
    }
  }

  cargarNumeroLetras(salario:number){
    this.validService.obtenerNumeroLetra(salario)
    .subscribe((res:any)=>{
      if(res.ok){
        this.numeroString = res.numeroLetra;
      }
    })
  }

}

const CONTRATO_CLAUSULA:Opcion_Clausula[] = [
  { id:1 , texto:' por día el cual le será cubierto en moneda de curso legal el día quince y el último día del mes, quedando comprendido dentro de dicha cantidad el pago del séptimo día y de los días de descanso obligatorio.<br>' 
                  +'Dicho salario será cubierto a EL TRABAJADOR el día quince y el último día del mes en moneda de curso legal.' },
  { id:2, texto:' por día el cual le será cubierto en moneda de curso legal cada semana, quedando comprendido dentro de dicha cantidad el pago del séptimo día y de los días de descanso obligatorio.' }
]

export interface Opcion_Clausula {
  id:number;
  texto:string;
}


