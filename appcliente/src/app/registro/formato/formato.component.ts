import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { RegistroService } from '../registro.service';
import { ActivatedRoute } from '@angular/router';
import { Colaborador } from 'src/app/models/Colaborador';

@Component({
  selector: 'app-formato',
  templateUrl: './formato.component.html',
  styleUrls: ['./formato.component.css']
})
export class FormatoComponent implements OnInit {
  colaborador:Colaborador = new Colaborador();
  @ViewChild('ponermaparef',{static:false}) ponermaparef : ElementRef;

  constructor(private regService:RegistroService, private route:ActivatedRoute) { }

  ngOnInit() {
    this.cargarColaborador();
  }

  cargarColaborador(){
    let idcolaborador = this.route.snapshot.paramMap.get('id');

    if(idcolaborador){
      this.regService.obtenerColaborador(Number(idcolaborador)).subscribe((res)=>{
        this.colaborador = res;
        this.ponermaparef.nativeElement.innerHTML = this.colaborador.html_mapa;
      });
    }
  }

  imprimirSeccion(nombreDiv:string){

      let contenido= document.getElementById(nombreDiv).innerHTML;
      let contenidoOriginal= document.body.innerHTML;
      document.body.innerHTML = contenido;  
      
      window.print();
      
      document.body.innerHTML = contenidoOriginal;
      location.reload();
      //console.log(document.body.innerHTML);
    }

}
