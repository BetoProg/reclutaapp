import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { FormGroup, FormControl, Validators, FormArray, AbstractControl } from '@angular/forms'
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, MatSelect } from '@angular/material';

import { Colaborador, Familiar, Exp_Laborales, Referencias } from '../models/Colaborador';
import { Puesto } from '../models/Puesto';
import { SelectItem } from '../models/Data';
import { AuthSerivce } from '../auth/auth.service';
import { Validacion } from '../models/Validaciones';
import { RegistroService } from './registro.service';
import { ValidacionService } from './validacion.service';
import { PuestosService } from './puestos.service';
import Swal from 'sweetalert2';

import * as _moment from 'moment';
import { default as _rollupMoment } from 'moment';
const moment = _rollupMoment || _moment;
//Constante iniciales para el formato de campos
export const MY_FORMATS = {
	parse: {
	  dateInput: 'LL',
	},
	display: {
	  dateInput: 'DD-MM-YYYY',
	  monthYearLabel: 'YYYY',
	  dateA11yLabel: 'LL',
	  monthYearA11yLabel: 'MM',
	},
};

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css'],
  providers:[ 
	{ provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]} ,
	{ provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
	Location, { provide: LocationStrategy, useClass: PathLocationStrategy }
   ]
})
export class RegistroComponent implements OnInit {
	//Variables iniciales
	@ViewChild('selectPuesto',{static:false}) selectPuesto: MatSelect;
	colaborador:Colaborador = new Colaborador();
	familiar:Familiar = new Familiar();
	validacion:Validacion = new Validacion();
	selectItem:SelectItem = new SelectItem();
	puesto:Puesto = new Puesto();
	form: FormGroup;
	fams:Familiar[] = [];
	expl:Exp_Laborales[] = [];
	refs:Referencias[] = [];
	puestos:Puesto[] = [];
	location: Location;
	isValid :boolean= false;
	isCheckBoxShow:boolean=false;
	isInputShow:boolean=false;
	isDisabled:boolean=false;
	showIconInvalid:boolean=false;
	tieneValidacion:boolean=false;
	esrecluta:boolean=false;
	mostrar_nombre_lic:boolean = false;
	titulo_placeholder:string='Nombre de Licenciatura';
	loading:boolean=false;
	band:any;
	ideditar:any;
	pathUrl:string

	//inyeccion de dependencias
	constructor(private regService:RegistroService,
		private validService:ValidacionService,
		private puestosService:PuestosService,
		private route:Router,
		private routep:ActivatedRoute,
		private authService:AuthSerivce,location: Location) { 
			this.location = location;
		}

	ngOnInit() {
		//iniciacion de formgroup rective forms
		this.form = new FormGroup({
			'nombre': new FormControl('', Validators.required),
			'apellido_paterno': new FormControl('', [Validators.required]),
			'apellido_materno': new FormControl('', [Validators.required]),
			'edad': new FormControl('', [Validators.required]),
			'estado_civil': new FormControl('', [Validators.required]),
			'curp': new FormControl('', [Validators.required, Validators.minLength(18)]),
			'rfc': new FormControl('', [ Validators.required,Validators.minLength(13) ]),
			'seguro_social': new FormControl('', [Validators.required,Validators.minLength(11)]),
			'fecha_const_nopenales': new FormControl('', [Validators.required]),
			'credito_Infonavit': new FormControl(''),
			'credito_fonacot': new FormControl(''),
			'correo_electronico': new FormControl('', [Validators.required,Validators.email]),
			'tel_movil': new FormControl('', [Validators.required,Validators.minLength(10),Validators.pattern("^[0-9]*$")]),
			'tel_casa': new FormControl('', [Validators.required,Validators.minLength(10),Validators.pattern("^[0-9]*$")]),
			'tipo_doc': new FormControl('', [Validators.required]),
			//'folio_doc': new FormControl('', [Validators.required]),
			'calle': new FormControl('', [Validators.required]),
			'no_ext': new FormControl('', [Validators.required]),
			'no_int': new FormControl(''),
			'colonia': new FormControl('', [Validators.required]),
			'municipio': new FormControl('', [Validators.required]),
			'cp': new FormControl('', [Validators.required]),
			'estado': new FormControl('', [Validators.required]),

			'es_alergia_med': new FormControl(''),
			'es_alergia_com': new FormControl(''),
			'enfermedad_cro': new FormControl(''),
			'desc_alergia_med': new FormControl('', [Validators.required]),
			'desc_alergia_com': new FormControl('', [Validators.required]),
			'desc_enfermedad_cro': new FormControl('', [Validators.required]),
			'nombre_contacto_emergencia': new FormControl('', [Validators.required]),
			'tel_contacto_emergencia':new FormControl('',[Validators.required,Validators.minLength(10),Validators.pattern("^[0-9]*$")]),
			'contacto_parentesco_emerg': new FormControl('', [Validators.required]),

			'ult_grado_est': new FormControl('', [Validators.required]),
			'nombre_lic_maestria': new FormControl('', [Validators.required]),
			'nombre_insti': new FormControl('', [Validators.required]),
			'doc_recibido': new FormControl('', [Validators.required]),
			'reg_profesional': new FormControl(''),
			'fecha_inicio': new FormControl('', [Validators.required]),
			'fecha_final': new FormControl('', [Validators.required]),
			'familiares': new FormArray([]),
			'ex_laborales': new FormArray([]),
			'referencias': new FormArray([]),

			'tiene_ref':new FormControl(''),
			'puesto':new FormControl('',[Validators.required]),
			'html_mapa': new FormControl('', [Validators.required,
				Validators.pattern('<iframe.+?src="(.+?)".+?<\/iframe>')]),

			//controles de validacion
			'c_nombre':new FormControl(''),
			'c_apellido_paterno':new FormControl(''),
			'c_apellido_materno':new FormControl(''),
			'c_edad':new FormControl(''),
			'c_estado_civil':new FormControl(''),
			'c_curp':new FormControl(''),
			'c_rfc':new FormControl(''),
			'c_seguro_social':new FormControl(''),
			'c_fecha_const_nopenales':new FormControl(''),
			'c_correo_electronico':new FormControl(''),
			'c_tel_movil':new FormControl(''),
			'c_tel_casa':new FormControl(''),
			'c_tipo_doc':new FormControl(''),
			'c_folio_doc':new FormControl(''),
			'c_calle':new FormControl(''),
			'c_no_ext':new FormControl(''),
			'c_no_int':new FormControl(''),
			'c_colonia':new FormControl(''),
			'c_municipio':new FormControl(''),
			'c_cp':new FormControl(''),
			'c_estado':new FormControl(''),
			'c_nombre_contacto_emergencia':new FormControl(''),
			'c_tel_contacto_emergencia':new FormControl(''),
			'c_contacto_parentesco_emerg':new FormControl(''),
			'c_ult_grado_est':new FormControl(''),
			'c_nombre_insti':new FormControl(''),
			'c_doc_recibido':new FormControl(''),
			'c_reg_profesional':new FormControl(''),
			'c_a_fecha_inicio':new FormControl(''),
			'c_a_fecha_final':new FormControl(''),
			'c_familares':new FormControl(''),
			'c_explaborales':new FormControl('')
		});

		this.cargarDatosColaborador();
	  }
	//carga de datos iniciales en formulario  
	cargarDatosColaborador(){
		this.ideditar = this.routep.snapshot.paramMap.get('id');
		this.pathUrl = this.location.path();

		if(this.pathUrl.includes('regnuevo')){
			this.isInputShow=true;
			this.esrecluta = true;
			return;
		}

		if(this.pathUrl.includes('validar')){
			this.isValid=true;
			this.isCheckBoxShow=true;
			this.isInputShow=false;
			this.listarPuestos();
		}

		if(this.pathUrl.includes('registro/editar')){
			this.isValid=true;
			this.esrecluta = true;
			this.isCheckBoxShow=false;
			this.showIconInvalid=true;
			this.isInputShow=true;
			this.isDisabled=true;
			this.listarPuestos();
		}

		this.obtenerValidacionColaborador(this.ideditar);
	}
	
	//envio de todos los datos del formulario
	onSubmit(){
		//envia la validacion se comento por que ya no se va usar
		/* if(this.pathUrl.includes('validar')){
			Swal.fire({
				title: 'Advertencia',
				text: "¿Está seguro de guardar las validaciones seleccionadas?",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'SI',
				cancelButtonText: "NO"
			  }).then((result) => {
				if (result.value) {
				  this.enviarValidacion();
				}else{
					this.route.navigate(['/colaboradores']);
				}
			});
		} */
		if(!this.form.valid){
			Swal.fire('Datos Incompletos','Te falta llenar algunos campos del formulario','error');
			console.log(this.form);
			return false;
		}else{
			//envia la actualizacion
			if(this.pathUrl.includes('registro/editar')){

				Swal.fire({
					title: 'Advertencia',
					text: "¿Está seguro de guardar las validaciones seleccionadas?",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'SI',
					cancelButtonText: "NO"
				}).then((result) => {
					if (result.value) {
						this.enviarActualizacionColaborador();
					}else{
						this.route.navigate(['/colaboradores']);
					}
				});
			}
			//envia un nuevo registro
			if(this.pathUrl.includes('regnuevo')){
				this.enviarRegistroColaborador();
			}
		}
	}

	listarPuestos(){
		this.puestosService.obtenerTodosPuestos().subscribe((res:any) =>
		{ 
			if(res.ok){
				this.puestos = res.puestos;
			}
		});
	}

	//metodo que envia datos del Colaborador
	private enviarRegistroColaborador(){
		this.fams = this.form.value.familiares;
		this.expl = this.form.value.ex_laborales;
		this.refs = this.form.value.referencias;

		if(this.fams.length > 0 || this.expl.length > 0 || this.refs.length > 0){
			this.setArrayModelo();
		}
		//se obitiene el usuario
		this.colaborador.usuario_creacion = 'recluta';

		this.regService.insertarRegistroCol(this.colaborador).subscribe((res:any)=>{
			if(res.ok){
				Swal.fire('Exito',res.mensaje,'success');
				this.form.reset();
			}
		});
	}

	//Metodo donde envia datos de colaborador para actualizacion
	private enviarActualizacionColaborador(){
		this.fams = this.form.value.familiares;
		this.expl = this.form.value.ex_laborales;
		this.refs = this.form.value.referencias;

		if(this.fams.length > 0 || this.expl.length > 0 || this.refs.length > 0){
			this.setArrayModelo();
		}

		//se obitiene el usuario
		let usuario = this.authService.loadUsuario();
		this.colaborador.usuario_creacion = usuario.username;
		
		if(this.selectPuesto.value){
			this.colaborador.descripcion_puesto = this.selectPuesto._elementRef.nativeElement.innerText.trim()
			this.colaborador.idpuesto = this.selectPuesto.value;
		}

		this.loading = true;
		this.regService.actualizarColaboradorValidador(this.colaborador,this.ideditar)
		.subscribe((res:any)=>{
			if(res.ok){
				Swal.fire('Exito','Se actulizo la solicitud con exito','success');
				this.loading = false;
				this.route.navigate(['/colaboradores']);
			}
		});
	}

	//Metodo que envia las validaciones datos booleanos
	private enviarValidacion(){

		//condicion cuando se actualiza
		if(this.tieneValidacion){
			this.validacion.es_valido = this.verficarValidacion(this.validacion);
			this.validacion.num_ediciones = this.validacion.num_ediciones + 1;
			
			if(!this.validacion){
				this.validacion.num_ediciones = 0;
			}

			let usuario = this.authService.loadUsuario();
			this.validacion.usuario_validacion = usuario.username;
			this.validService.actualizarValidacion(this.validacion, this.ideditar)
			.subscribe((res:any)=>{
				if(res.ok){
					this.enviarDatosPuestoColaborador();
					Swal.fire('Exito en Validacion','Se actualizo registro correctamente','success');
					this.route.navigate(['/colaboradores']);
				}
			});
		}else{
			//condicion cuando es nueva validacion
			this.validacion.idcolaborador=this.ideditar;
			this.validacion.es_valido = true;
			this.validacion.num_ediciones = 0;
			let usuario = this.authService.loadUsuario();
			this.validacion.usuario_validacion = usuario.username;
			this.loading = true;
			this.validService.insertValidacion(this.validacion).subscribe((res:any)=>{
				if(res.ok){
					this.enviarDatosPuestoColaborador();
					Swal.fire('Nueva Validacion',res.mensaje,'success');
					this.route.navigate(['/colaboradores']);
					this.loading = true;
				}
			},error=>{
				this.loading = false;
			}); 
		}
	}

	//metodo que setea el modelo Puesto para actualizarlo
	private enviarDatosPuestoColaborador(){
		this.puesto.descripcion = this.selectPuesto._elementRef.nativeElement.innerText.trim()
		this.puesto.idpuesto = this.selectPuesto.value;

		this.regService.actualizarColaboradorPuesto(this.puesto,this.ideditar).subscribe();
	}

	//Metodo que optiene validacion del colaborador
	private obtenerValidacionColaborador(id:any){
		this.regService.obtenerColaborador(id).subscribe((res)=>{
			this.colaborador = res;
			this.cargarFormFamiliares();
			this.cargarFormExpLaborales();
			this.cargarFormReferencias();
		});

		
		//se optiene validacion se comenta ya no se usara
		// this.validService.verificarValidaciones(id).subscribe((res:any)=>{
		// 	if(res.validador){
		// 		this.validacion = res.validador;
		// 		this.tieneValidacion = true;
		// 	}
		// });
	}
	//Metodo donde setea los arreglos de familiares, exp_laboral y referencias
	private setArrayModelo(){
		this.colaborador.Familiares = [];
		this.colaborador.Exp_Laborales = [];
		this.colaborador.Referencias = [];
		
		this.fams.forEach(element => {
			//console.log(element);
			this.colaborador.Familiares.push(element);
		});

		this.expl.forEach(element => {
			//console.log(element);
			this.colaborador.Exp_Laborales.push(element);
		});

		this.refs.forEach(element=> {
			this.colaborador.Referencias.push(element);
		})
		

		//console.log(this.colaborador.Familiares);
	}
	//metodo que controla si tiene refencia o no
	checkEs_Ref(event:any){
		if(event){
			(<FormArray>this.form.controls['ex_laborales']) = new FormArray([]);
		}else{
			(<FormArray>this.form.controls['referencias'])= new FormArray([]);
		}
	}
	//Metodo que añade controles de array Familares
	anadirFamiliar() {
		(<FormArray>this.form.controls['familiares']).push(new FormGroup({
		  'nombre': new FormControl(''),
		  'ape_paterno': new FormControl(''),
		  'ape_materno': new FormControl(''),
		  'parentesco': new FormControl(''),
		  'fecha_nacimiento': new FormControl('')
		}));
	}

	//Metodo que añade controles de array Exp_Laborales y Referencias
	anadirExpLaboral() {

		if(!this.colaborador.tiene_ref){
			(<FormArray>this.form.controls['ex_laborales']).push(new FormGroup({
				'nombre_emp': new FormControl(''),
				'puesto': new FormControl(''),
				'fecha_inicio': new FormControl(''),
				'fecha_fin': new FormControl(''),
				'causa_salida': new FormControl(''),
				'otro_causa_sal': new FormControl(''),
				'nombre_jefe': new FormControl(''),
				'tel_contacto': new FormControl('')
			  }));

			  (<FormArray>this.form.controls['referencias']).removeAt(0);
		}else{
			(<FormArray>this.form.controls['referencias']).push(new FormGroup({
				'nombre': new FormControl(''),
				'telefono_contacto': new FormControl('',[Validators.minLength(10),Validators.pattern("^[0-9]*$")])
			}));

			(<FormArray>this.form.controls['ex_laborales']).removeAt(0);
		}
		
	}
	//Metodos que realizan la carga incial de form Arrar de Familiares, Exp_Laborales y Referencias
	cargarFormFamiliares(){

		this.colaborador.Familiares.forEach(e => {
			(<FormArray>this.form.controls['familiares']).push(
				new FormGroup({
					'nombre': new FormControl(e.nombre),
					'ape_paterno': new FormControl(e.ape_paterno),
					'ape_materno': new FormControl(e.ape_materno),
					'parentesco': new FormControl(e.parentesco),
					'fecha_nacimiento': new FormControl(e.fecha_nacimiento)
				  })
			)
		});
		
	}

	cargarFormExpLaborales(){
		this.colaborador.Exp_Laborales.forEach(e=>{
			(<FormArray>this.form.controls['ex_laborales']).push(new FormGroup({
				'nombre_emp': new FormControl(e.nombre_emp),
				'puesto': new FormControl(e.puesto),
				'fecha_inicio': new FormControl(e.fecha_inicio),
				'fecha_fin': new FormControl(e.fecha_fin),
				'causa_salida': new FormControl(e.causa_salida),
				'otro_causa_sal': new FormControl(e.otro_causa_sal),
				'nombre_jefe': new FormControl(e.nombre_jefe),
				'tel_contacto': new FormControl(e.tel_contacto)
			  }));
		})
	}

	cargarFormReferencias(){
		this.colaborador.Referencias.forEach(e=>{
			(<FormArray>this.form.controls['referencias']).push(new FormGroup({
				'nombre': new FormControl(e.nombre),
				'telefono_contacto': new FormControl(e.telefono_contacto)
			}));
		})
	}
	//Metodos 	que remueven el control seleccionado
	removerFormFamiliares(index:number){
		(<FormArray>this.form.controls['familiares']).removeAt(index);
	}

	removerFormExp_Laboral(index:number){
		(<FormArray>this.form.controls['ex_laborales']).removeAt(index);
	}

	removerFormReferencias(index:number){
		(<FormArray>this.form.controls['referencias']).removeAt(index);
	}

	//Metodo que valida en conjunto si tiene validaciones correctas
	private verficarValidacion(v:Validacion):boolean{
		return v.c_nombre || 
		v.c_apellido_paterno || 
		v.c_apellido_materno || 
		v.c_edad || 
		v.c_estado_civil || 
		v.c_curp || 
		v.c_rfc || 
		v.c_seguro_social || 
		v.c_fecha_const_nopenales || 
		v.c_correo_electronico || 
		v.c_tel_movil || 
		v.c_tel_casa || 
		v.c_tipo_doc || 
		v.c_folio_doc || 
		v.c_calle || 
		v.c_no_ext || 
		v.c_no_int || 
		v.c_colonia || 
		v.c_municipio || 
		v.c_cp || 
		v.c_estado || 
		v.c_nombre_contacto_emergencia || 
		v.c_tel_contacto_emergencia || 
		v.c_contacto_parentesco_emerg || 
		v.c_ult_grado_est || 
		v.c_nombre_insti || 
		v.c_doc_recibido || 
		v.c_reg_profesional || 
		v.c_a_fecha_inicio ||
		v.c_a_fecha_final || 
		v.c_familares || 
		v.c_explaborales; 
	}

	fechaMaximaMinStart(date?:string){

		if(date){
			const fecha = moment(date,"DD-MM-YYYY");
			return new Date(
				fecha.year(),
				fecha.month(),
				fecha.day()
			);
		}else{
			return new Date();
		}

	}

	validarFechaMayor(fecha_inicial:string,fecha_final:string){
		return(group:FormGroup) => {
			let f_inicial = group.controls[fecha_inicial].value;
			let f_final = group.controls[fecha_final].value;
			if(f_inicial){

			}
			return{
				validarFechaMayor:true
			} 
		}
	}
	
	mostrarInputNombreLic(item:any){
		if(item === 'Licenciatura'){
			this.titulo_placeholder= "Nombre de Licenciatura"
			this.mostrar_nombre_lic = true;
		}else if(item === 'Maestria'){
			this.titulo_placeholder= "Nombre de Maestria"
			this.mostrar_nombre_lic = true;
		}else{
			this.mostrar_nombre_lic = false;
			this.form.get('nombre_lic_maestria').setValidators = null;
		}
	}

}
