import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { catchError, map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import Swal from 'sweetalert2';
import { Puesto } from '../models/Puesto';

@Injectable({
    providedIn:'root'
})
export class PuestosService {
    url:string = environment.url;

    constructor(private http:HttpClient){

    }

    obtenerTodosPuestos(){
        return this.http.get(`${this.url}/puestos`)
        .pipe(
            catchError(err => {
                Swal.fire('Error en Obtener Lista de Puestos',err.error.mensaje,'error');
                return Observable.throw(err);
            })
        );
    }
}