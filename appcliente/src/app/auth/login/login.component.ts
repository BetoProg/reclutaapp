import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthSerivce } from '../auth.service';
import { LoginDto, LoginCurp } from 'src/app/models/Auth';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  valid:any;
  mostrar=false;
  mostrarSpinner = false;
  curp:string;

  constructor(
    private authService:AuthSerivce, 
    private router:Router,private routerp:ActivatedRoute) { }

  ngOnInit() {
    this.valid = this.routerp.snapshot.paramMap.get('curp');

    if(this.valid){
      this.mostrar=true;
      this.curp = this.valid;
    }

  }

  onSubmit(f:NgForm){
    const logeo = new LoginDto();
    logeo.username = f.value.username;
    logeo.password = f.value.password;
    logeo.curp = f.value.curp;
    this.mostrarSpinner = true;

    if(logeo.curp){
      const logeoCurp = new LoginCurp();
      logeoCurp.curp = logeo.curp;
      this.authService.loginCurp(logeoCurp).subscribe((res:any)=>{
        //const usuario = this.authService.loadUsuarioCurp();
        if(res){
          const usuario = this.authService.loadUsuarioCurp();
          this.mostrarSpinner = false;
          this.router.navigate(['/registro/editar',usuario.idcolaborador]);
        }
      },error=>{
        this.mostrarSpinner = false;
      })
      
    }else{
      setTimeout(()=>{
        this.authService.login(logeo).subscribe((res:any)=>{
          const usuario = this.authService.loadUsuario();
  
          if(res.ok){
  
            if(usuario.perfil==='admin'){
              this.router.navigate(['/colaboradores']);
            }else{
              Swal.fire('Acceso Denegado','Usuario invalido','error');
              this.mostrarSpinner = false;
              this.authService.logout();
            } 
          }
          
        },error=>{
          this.mostrarSpinner = false;
          this.authService.logout();
        });
      },1500)
      
    }
  }

}
