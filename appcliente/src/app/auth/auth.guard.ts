import { CanActivate, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthSerivce } from './auth.service';

@Injectable({
    providedIn:'root'
})
export class AuthGuard implements CanActivate{
    constructor(private authService:AuthSerivce, private route:Router){

    }

    canActivate(): boolean  {
        if(this.authService.loggedIn()){
            return true;
        }
        
        this.route.navigate(['/login']);
        return false;
    }

}