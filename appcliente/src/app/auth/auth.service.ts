import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LoginDto, Usuario, LoginCurp } from '../models/Auth';
import { environment } from '../../environments/environment';
import { map, catchError } from 'rxjs/operators'
import { Observable } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import Swal from 'sweetalert2'
import { Router } from '@angular/router';

@Injectable({
    providedIn:'root'
})
export class AuthSerivce {
    usuario:Usuario = new Usuario();
    url:string = environment.url;
    header = new HttpHeaders({ 'Content-Type':'application/json' });
    jwtHelper = new JwtHelperService();


    constructor(private http:HttpClient, private route:Router){

    }

    login(login:LoginDto){
        return this.http.post(`${this.url}/auth/login`,login,{ headers:this.header})
        .pipe(
            map((res:any)=>{
                
                localStorage.setItem('usuario',JSON.stringify(res.usuario));
                localStorage.setItem('token',res.token);
                return res;
            }),
            catchError(err=>{
                Swal.fire('Error',err.error.mensaje,'error');
                return Observable.throw(err);
            })
        );
    }

    loginCurp(curp:LoginCurp ){
        return this.http.post(`${this.url}/auth/login_curp`,curp,{headers:this.header})
        .pipe(
            map((res:any)=>{
                localStorage.setItem('usuario',JSON.stringify(res.usuario));
                localStorage.setItem('token',res.token);
                return true;
            }),
            catchError(err=>{
                Swal.fire('Error',err.error.mensaje,'error');
                return Observable.throw(err);
            })
        );
    }

    logout(){
        localStorage.removeItem('token');
        localStorage.removeItem('usuario');
        this.route.navigate(['/login']);
    }

    loggedIn():boolean{
        const token = localStorage.getItem('token');
        return !this.jwtHelper.isTokenExpired(token)
    }

    isLogin(){
        return (localStorage.getItem('usuario')) ? true:false;
    }

    loadUsuario():Usuario{
        this.usuario = JSON.parse(localStorage.getItem('usuario'));
        return this.usuario;
    }

    loadUsuarioCurp(){
        const usuario = JSON.parse(localStorage.getItem('usuario'));
        return usuario;
    }
    
}