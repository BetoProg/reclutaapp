import { NgModule } from '@angular/core';
import { CdkTableModule } from '@angular/cdk/table';
import {CdkTreeModule} from '@angular/cdk/tree';
import { 
    MatButtonModule, 
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatSidenavModule,
    MatToolbarModule,
    MatListModule,
    MatCardModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCheckboxModule,
    MatRadioModule,
    MatTableModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatBadgeModule
} from '@angular/material';

@NgModule({
    imports:[
        CdkTableModule,
        CdkTreeModule,
        MatButtonModule,
        MatIconModule,
        MatFormFieldModule,
        MatInputModule,
        MatSidenavModule,
        MatToolbarModule,
        MatListModule,
        MatCardModule,
        MatSelectModule,
        MatDatepickerModule, 
        MatNativeDateModule,
        MatCheckboxModule,
        MatRadioModule,
        MatTableModule,
        MatPaginatorModule,
        MatProgressSpinnerModule,
        MatProgressBarModule,
        MatBadgeModule
    ],
    exports:[
        CdkTableModule,
        CdkTreeModule,
        MatButtonModule,
        MatIconModule,
        MatFormFieldModule,
        MatInputModule,
        MatSidenavModule,
        MatToolbarModule,
        MatListModule,
        MatCardModule,
        MatSelectModule,
        MatDatepickerModule, 
        MatNativeDateModule,
        MatCheckboxModule,
        MatRadioModule,
        MatTableModule,
        MatPaginatorModule,
        MatProgressSpinnerModule,
        MatProgressBarModule,
        MatBadgeModule
    ]
})
export class MaterialModule{} 