import { BrowserModule } from '@angular/platform-browser';
import { registerLocaleData } from '@angular/common';
import { NgModule, LOCALE_ID } from '@angular/core';
import localeEs from '@angular/common/locales/es';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { NgxPrintModule } from 'ngx-print';
import { AppRoutingModule } from './app-routing.module';
import { MaterialModule } from './material.module';

//pipes
import { BooleanosPipe } from './pipes/booleanos.pipe';
//directivas
import { DisabledControlDirective } from './registro/disabled.directive';
//Componentes
import { AppComponent } from './app.component';
import { LoginComponent } from './auth/login/login.component';
import { RegistroComponent } from './registro/registro.component';
import { HeaderComponent } from './navigation/header/header.component';
import { SidenavComponent } from './navigation/sidenav/sidenav.component';
import { FormatoComponent } from './registro/formato/formato.component';
import { ListaColaboradoresComponent } from './registro/lista-colaborador/lista-colaborador.component';
import { FormularioComponent } from './registro/formulario/formulario.component';
import { ContratoComponent } from './registro/formato/contrato/contrato.component';
import { SpinnerComponent } from './spinner/spinner.component';
import { MapaComponent } from './mapa/mapa.component';

/*metodo que traduce las fechas a español*/
registerLocaleData(localeEs);

@NgModule({
  declarations: [
    DisabledControlDirective,
    AppComponent,
    LoginComponent,
    RegistroComponent,
    HeaderComponent,
    SidenavComponent,
    FormatoComponent,
    ListaColaboradoresComponent,
    FormularioComponent,
    ContratoComponent,
    SpinnerComponent,
    MapaComponent,
    BooleanosPipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    NgxPrintModule
  ],
  providers: [
    { provide:LOCALE_ID,useValue:'es-MX' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
