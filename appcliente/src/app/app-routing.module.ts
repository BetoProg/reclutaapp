import { NgModule } from '@angular/core';
import { AuthGuard } from './auth/auth.guard';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { RegistroComponent } from './registro/registro.component';
import { FormatoComponent } from './registro/formato/formato.component';
import { ListaColaboradoresComponent } from './registro/lista-colaborador/lista-colaborador.component';
import { ContratoComponent } from './registro/formato/contrato/contrato.component';

const routes: Routes = [
  { path:'login', component:LoginComponent },
  { path:'login/:curp', component:LoginComponent },
  { path:'regnuevo', component:RegistroComponent },
  { path:'validar/:id', component:RegistroComponent,canActivate:[AuthGuard] },
  { path: 'registro/editar/:id',component:RegistroComponent,canActivate:[AuthGuard] },
  { path:'formato/:id', component:FormatoComponent,canActivate:[AuthGuard] },
  { path: 'contrato/:id', component:ContratoComponent,canActivate:[AuthGuard] },
  { path:'colaboradores', component:ListaColaboradoresComponent,canActivate:[AuthGuard] },
  { path:'**', redirectTo:'regnuevo', pathMatch:'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
