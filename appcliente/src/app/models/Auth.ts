export class LoginDto {
    public username:string;
    public password:string;
    public curp?:string;
}

export class LoginCurp {
    public curp:string;
}

export class Usuario {
    public username:string;
    public perfil:string;
}