export class SelectItem{
    estados_civiles:SelectItems[] = [
        { titulo:'Casado',value:'casado' },
        { titulo:'Soltero',value:'soltero' },
        { titulo:'Unión Libre',value:'union_libre' }
    ]

    tipos_documentos:SelectItems[] = [
        { titulo:'Credencial de Elector',value:'Credencial_Elector' },
        { titulo:'Cartilla Militar',value:'Cartilla_Militar' },
        { titulo:'Cédula Profesional',value:'Cedula_Profesional' },
        { titulo:'Pasaporte',value:'Pasaporte' },
    ]

    parentescos:SelectItems[]=[
        { titulo:'Hijo(a)',value:'hijo' },
        { titulo:'Hermano(a)',value:'hermano' },
        { titulo:'Esposo(a)',value:'esposo' },
        { titulo:'Padre',value:'padre' },
        { titulo:'Madre',value:'madre'}
    ]

    documentos_recibidos:SelectItems[] = [
        { titulo:'Certificado',value:'Certificado' },
        { titulo:'Título',value:'Titulo' },
        { titulo:'Diploma',value:'Diploma' },
        { titulo:'Carta de pasante',value:'Carta_pasante' }
    ]

    causas_salidas:SelectItems[]=[
        { titulo:'Renuncia Voluntaria',value:'renuncia_voluntaria' },
        { titulo:'Término de contrato',value:'termino_contrato' },
        { titulo:'Recisión de contrato',value:'recision_contrato' },
        { titulo:'Abandono de empleo',value:'abandono_empleo' }
    ]

    ultimos_grados_estudios:SelectItems[]=[
        { titulo:'Secundaria',value:'Secundaria' },
        { titulo:'Bachillerato',value:'Bachillerato' },
        { titulo:'Licenciatura',value:'Licenciatura' },
        { titulo:'Maestria',value:'Maestria' },
    ]
}

interface SelectItems {
    titulo:string;
    value:string;
}