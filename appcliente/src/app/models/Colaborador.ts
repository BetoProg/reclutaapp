export class Colaborador {
    public nombre :string;
	public apellido_paterno : string;
	public apellido_materno : string;
	public edad : number;
	public estado_civil : string;
	public curp : string;
	public rfc : string;
	public seguro_social : string;
	public fecha_const_nopenales : Date;
	public credito_Infonavit : boolean;
	public credito_fonacot : boolean;
	public correo_electronico : string;
	public tel_movil : string;
	public tel_casa : string;
	public tipo_doc : string;
	public folio_doc : string;
	public calle : string;
	public no_ext : string;
	public no_int : string;
	public colonia : string;
	public municipio : string;
	public cp : string;
	public estado : string;
	public usuario_creacion : string
	public tiene_ref:boolean;
	public idpuesto:string;
	public descripcion_puesto:string;
	public salario_referencia:number;
	public salario_mensual:number;
	public html_mapa:string;
    
    //propiedades salud
    public es_alergia_med : boolean;
	public desc_alergia_med : string;
	public es_alergia_com : boolean;
	public desc_alergia_com : string;
	public enfermedad_cro : boolean;
	public desc_enfermedad_cro : string;
	public nombre_contacto_emergencia : string;
	public tel_contacto_emergencia : string;
    public contacto_parentesco_emerg : string;
    
    //propiedades academicas
	public ult_grado_est : string;
	public nombre_lic_maestria:string;
	public nombre_insti : string;
	public doc_recibido : string;
	public reg_profesional : string;
	public fecha_inicio : Date;
    public fecha_final:Date;

    public Familiares:Familiar[]=[];
	public Exp_Laborales:Exp_Laborales[]=[];
	public Referencias:Referencias[] = [];

}

export class Familiar{
    public nombre : string;
    public ape_paterno : string;
    public ape_materno : string;
    public parentesco : string;
    public fecha_nacimiento : Date;
}

export class Exp_Laborales {
    public nombre_emp : string;
    public puesto : string;
    public fecha_inicio : Date;
    public fecha_fin : Date;
    public causa_salida : string;
    public otro_causa_sal : string;
    public nombre_jefe : string;
    public tel_contacto : string;
}

export class Referencias {
	public nombre:string;
	public telefono_contacto:string;
}

export class ListarColaborador{
	public idcolaborador:number;
	public num_ediciones:number;
	public es_valido:boolean;
	public nombre :string;
	public apellido_paterno : string;
	public apellido_materno : string;
	public correo_electronico : string;
	public tieneValidacion:boolean;
	public fecha_creacion: Date;
}