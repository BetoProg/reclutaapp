﻿using ReclutamientoFonix.Data.Repository;
using ReclutamientoFonix.Data.RepositorySoft;
using ReclutamientoFonix.Seguridad;
using ReclutamientoFonix.Services;
using SimpleInjector;
using SimpleInjector.Integration.WebApi;
using SimpleInjector.Lifestyles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ReclutamientoFonix
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Configuración y servicios de API web
            var container = new Container();

            container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();
            //Registro de dependencias
            container.Register<IUnitWork, UnitWork>(Lifestyle.Scoped);
            container.Register<IUnitWorkSoftland, UnitWorkSoftland>(Lifestyle.Scoped);
            container.Register<IEmailSender, EmailSender>(Lifestyle.Scoped);

            container.RegisterWebApiControllers(GlobalConfiguration.Configuration);
            container.Verify();

            GlobalConfiguration.Configuration.DependencyResolver = new SimpleInjectorWebApiDependencyResolver(container);

            // Rutas de API web
            config.MapHttpAttributeRoutes();

            //Enable cors
            var cors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);

            config.MessageHandlers.Add(new TokenValidationHandler());

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
