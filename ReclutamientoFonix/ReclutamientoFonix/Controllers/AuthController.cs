﻿using ReclutamientoFonix.Data;
using ReclutamientoFonix.Data.Repository;
using ReclutamientoFonix.Dto;
using ReclutamientoFonix.Seguridad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace ReclutamientoFonix.Controllers
{
    [RoutePrefix("api/auth")]
    public class AuthController : ApiController
    {
        private readonly IUnitWork _unitWork;

        public AuthController(IUnitWork unitWork)
        {
            _unitWork = unitWork;
        }

        [HttpPost]
        [Route("login")]
        public async Task<HttpResponseMessage> Login(LoginDto model)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, 
                    new {
                        ok=false,
                        mensaje = "Los datos del usuario son invalidos"
                    });
            }


            var usuario = await _unitWork.AuthRepository.GetUsuario(model);
            if (usuario == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, 
                    new {
                        ok=false,
                        mensaje = "El usuario ingresado no se encuentra registrado"
                    });
            }

            var token = TokenGenerator.GetTokenJwt(usuario.username);

            return Request.CreateResponse(HttpStatusCode.Accepted, new UsuarioReturnDto
            {
                ok=true,
                usuario = usuario,
                token = token
            });

        }

        [HttpPost]
        [Route("login_curp")]
        public async Task<HttpResponseMessage> LoginCurp(LoginPorCurp model)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { mensaje = "Los datos del usuario son invalidos" });
            }

            var usuario = await _unitWork.AuthRepository.GetUsuarioCurp(model);
            if (usuario==null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, new { mensaje = "El usuario ingresado no se encuentra registrado" });
            }

            var token = TokenGenerator.GetTokenJwt("usuarioporcurp");

            return Request.CreateResponse(HttpStatusCode.Accepted, new
            {
                ok =true,
                usuario,
                token
            });
        }
    }
}
