﻿using ReclutamientoFonix.Data.Repository;
using ReclutamientoFonix.Data.RepositorySoft;
using ReclutamientoFonix.Dto;
using ReclutamientoFonix.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace ReclutamientoFonix.Controllers
{
    [RoutePrefix("api/colaborador")]
    public class ColaboradorController : ApiController
    {
        private readonly IUnitWork _unitWork;
        private readonly IUnitWorkSoftland _unitWorkSoftland;
        private readonly IEmailSender _emailSender;

        public ColaboradorController(IUnitWork unitWork, IEmailSender emailSender, IUnitWorkSoftland unitWorkSoftland)
        {
            _unitWork = unitWork;
            _unitWorkSoftland = unitWorkSoftland;
            _emailSender = emailSender;
        }

        [HttpPost]
        [Route("nuevo")]
        public async Task<HttpResponseMessage> nuevoColaborador(ColaboradorRegistro model)
        {
            if (!ModelState.IsValid)
            {
                var errorList = ModelState.Values.SelectMany(m => m.Errors)
                                 .Select(e => e.ErrorMessage)
                                 .ToList();

                return Request.CreateResponse(HttpStatusCode.BadRequest,
                    new {
                        ok = false,
                        mensaje = "Los Datos enviados son invalidos",
                        model_errors = errorList
                    });
            }

            int idcolaborador = await _unitWork.ColaboradorRepository.InsertColaborar(model);

            await _unitWork.ColaboradorRepository.InsertSalud(model, idcolaborador);
            await _unitWork.ColaboradorRepository.InsertAcademico(model, idcolaborador);

            foreach (var familiar in model.Familiares)
            {
                await _unitWork.ColaboradorRepository.InsertFamiliares(familiar, idcolaborador);
            }

            foreach (var expLaboral in model.Exp_Laborales)
            {
                await _unitWork.ColaboradorRepository.InsertExp_Laboral(expLaboral, idcolaborador);
            }

            foreach (var referencia in model.Referencias)
            {
                await _unitWork.ColaboradorRepository.InsertReferencias(referencia, idcolaborador);
            }

            if (!await _emailSender.SendEmailAsync(idcolaborador)) {
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                     new
                     {
                         ok = false,
                         mensaje = "Ocurrio un error en el envio del correo, no se hizo inserción",
                     });
            }

            _unitWork.Commit();
            return Request.CreateResponse(HttpStatusCode.Accepted,
                new { ok = true, mensaje = "Se ha ingresado correctamente, y se envio correo a area de Reclutamiento" }
            );

        }

        [HttpGet]
        [Route("obtener/{id}")]
        public async Task<HttpResponseMessage> obtenerColaborador(int id)
        {
            var colaborador = await _unitWork.ColaboradorRepository.ObtenerColaborador(id);
            var familiares = await _unitWork.ColaboradorRepository.ObtenerFamiliares(id);
            var exp_laboral = await _unitWork.ColaboradorRepository.ObtenerExp_Laborales(id);
            var referencias = await _unitWork.ColaboradorRepository.ObetenerReferencias(id);

            if (colaborador == null)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { ok = false, mensaje = "Los Datos enviados son invalidos" });
            }

            colaborador.Familiares = familiares.ToList();
            colaborador.Exp_Laborales = exp_laboral.ToList();
            colaborador.Referencias = referencias.ToList();

            return Request.CreateResponse(HttpStatusCode.Accepted,
                new { ok = true, colaborador }
            );
        }

        [HttpGet]
        [Route("todos")]
        public async Task<HttpResponseMessage> obtenerColaboradores()
        {
            var listacolaboradores = await _unitWork.ColaboradorRepository.ObtenerColaboradores();
            List<Reclutados> colaboradores = new List<Reclutados>();

            foreach (var col in listacolaboradores)
            {
                var validacion = await _unitWork.ValidarRepository.ObtenerValidacion(col.idcolaborador);

                colaboradores.Add(new Reclutados {
                    idcolaborador = col.idcolaborador,
                    es_valido = (validacion != null) ? validacion.es_valido : false,
                    num_ediciones = (validacion != null) ? validacion.num_ediciones : 0,
                    nombre = col.nombre,
                    apellido_paterno = col.apellido_paterno,
                    apellido_materno = col.apellido_materno,
                    correo_electronico = col.correo_electronico,
                    tieneValidacion = (!string.IsNullOrEmpty(col.html_mapa)) ? true : false,
                    fecha_creacion = col.fecha_creacion
                });
            }

            return Request.CreateResponse(HttpStatusCode.Accepted,
                new { ok = true, colaboradores }
            );
        }

        [HttpPut]
        [Route("actualizar/{idcolaborador}")]
        public async Task<HttpResponseMessage> actualizarTodoColaborador(ColaboradorRegistro model, int idcolaborador)
        {
            if (!ModelState.IsValid)
            {
                var errorList = ModelState.Values.SelectMany(m => m.Errors)
                                 .Select(e => e.ErrorMessage)
                                 .ToList();

                return Request.CreateResponse(HttpStatusCode.BadRequest,
                    new
                    {
                        ok = false,
                        mensaje = "Los Datos enviados son invalidos",
                        model_errors = errorList
                    });
            }

            var colaborador = await _unitWork.ColaboradorRepository.ObtenerColaborador(idcolaborador);
            var familiares = await _unitWork.ColaboradorRepository.ObtenerFamiliares(idcolaborador);
            var exp_laborales = await _unitWork.ColaboradorRepository.ObtenerExp_Laborales(idcolaborador);
            var referencias = await _unitWork.ColaboradorRepository.ObetenerReferencias(idcolaborador);
            var puesto = await _unitWorkSoftland.PuestosRepository.ObtenerPuesto(model.idpuesto);

            if(puesto != null)
            {
                model.salario_referencia = puesto.salario_referencia;
                model.salario_mensual = puesto.salario_mensual;
            }


            if (colaborador != null)
            {
                await _unitWork.ColaboradorRepository.Update_Colaborador(model, colaborador.idcolaborador);
            }

            if (familiares.Count() == 0)
            {
                foreach (var familiar in model.Familiares)
                    await _unitWork.ColaboradorRepository.InsertFamiliares(familiar, idcolaborador);
            }
            else
            {
                await _unitWork.ColaboradorRepository.Delete_Familiares(idcolaborador);
                foreach (var familiar in model.Familiares)
                    await _unitWork.ColaboradorRepository.InsertFamiliares(familiar, idcolaborador);
            }
                

            if (exp_laborales.Count() == 0)
            {
                foreach (var expLaboral in model.Exp_Laborales)
                    await _unitWork.ColaboradorRepository.InsertExp_Laboral(expLaboral, idcolaborador);
            }
            else
            {
                await _unitWork.ColaboradorRepository.Delete_Exp_Laborales(idcolaborador);
                foreach (var laboral in model.Exp_Laborales)
                    await _unitWork.ColaboradorRepository.InsertExp_Laboral(laboral, idcolaborador);
            }

            if (referencias.Count() == 0)
            {
                foreach (var referencia in model.Referencias)
                    await _unitWork.ColaboradorRepository.InsertReferencias(referencia, idcolaborador);
            }
            else
            {
                await _unitWork.ColaboradorRepository.Delete_Referencias(idcolaborador);
                foreach (var referencia in model.Referencias)
                    await _unitWork.ColaboradorRepository.InsertReferencias(referencia, idcolaborador);
            }

            //await _unitWork.ValidarRepository.Update_Validaciones(model.validaciones,idcolaborador);

            //se confirman los datos en la base de datos
            _unitWork.Commit();

            if (!await _emailSender.SendEmailAsync(idcolaborador))
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                     new
                     {
                         ok = false,
                         mensaje = "Ocurrio un error en el envio del correo, no se hizo la actualizacion",
                     });
            }

            return Request.CreateResponse(HttpStatusCode.Accepted,
                new { ok = true, mensaje = "se actualizo correctamente" }
            );
        }

        [HttpPut]
        [Route("actualizar/puesto/{idcolaborador}")]
        public async Task<HttpResponseMessage> actualizarPuestoColaborador(Puesto model, int idcolaborador)
        {
            await _unitWork.ColaboradorRepository.Update_ColaboradorPuesto(model,idcolaborador);
            _unitWork.Commit();

            return Request.CreateResponse(HttpStatusCode.Accepted,
                new { ok = true, mensaje = "se actualizo correctamente" }
            );
        } 
    }
}
