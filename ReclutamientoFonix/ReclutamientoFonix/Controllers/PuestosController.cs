﻿using ReclutamientoFonix.Data.RepositorySoft;
using ReclutamientoFonix.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace ReclutamientoFonix.Controllers
{
    [RoutePrefix("api/puestos")]
    public class PuestosController : ApiController
    {
        private readonly IUnitWorkSoftland _unitWorkSoftland;

        public PuestosController(IUnitWorkSoftland unitWorkSoftland)
        {
            _unitWorkSoftland = unitWorkSoftland;
        }

        [HttpGet]
        public async Task<HttpResponseMessage> obtenerPuestos()
        {
            List<Puesto> puestos = new List<Puesto>();

            try
            {
                var puestosRepo = await _unitWorkSoftland.PuestosRepository.ObtenerPuestosEmpleados();
                puestos = puestosRepo.ToList();
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.Accepted, new
                {
                    ok = false,
                    mensaje = "Error de Servidor: " + ex.Message
                });

            }

            return Request.CreateResponse(HttpStatusCode.Accepted, new
            {
                ok = true,
                puestos
            });
        }
    }      

}

