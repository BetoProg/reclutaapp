﻿using ReclutamientoFonix.Data.Repository;
using ReclutamientoFonix.Dto;
using ReclutamientoFonix.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace ReclutamientoFonix.Controllers
{
    [RoutePrefix("api/validacion")]
    public class ValidadorController : ApiController
    {
        private readonly IUnitWork _unitWork;
        private readonly IEmailSender _emailSender;

        public ValidadorController(IUnitWork unitWork,IEmailSender emailSender)
        {
            _unitWork = unitWork;
            _emailSender = emailSender;
        }

        [HttpPost]
        [Route("nuevo")]
        public async Task<HttpResponseMessage> nuevoValidador(ValidacionesRegistro model)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { ok = false, mensaje = "Los Datos enviados son invalidos" });
            }

            await _unitWork.ValidarRepository.InsertValidacionReg(model);
            _unitWork.Commit();

            return Request.CreateResponse(HttpStatusCode.Accepted,
                new { ok = true, mensaje = "Se ha ingresado nuevo registro de validacion" }
            );
        }

        [HttpGet]
        [Route("tienevalidacion/{idcolaborador}")]
        public async Task<HttpResponseMessage> tieneValidador(int idcolaborador)
        {

            var validador = await _unitWork.ValidarRepository.ObtenerValidacion(idcolaborador);

            if (validador == null)
            {
                return Request.CreateResponse(HttpStatusCode.Accepted,
                    new {
                        ok = false,
                        mensaje= "No cuenta con validaciones"
                    }
                );
            }

            return Request.CreateResponse(HttpStatusCode.Accepted,
                new { ok = true, validador }
            );
        }

        [HttpGet]
        [Route("numeroletras")]
        public async Task<HttpResponseMessage> numeroletras(decimal numero)
        {
            var numeroLetra = await _unitWork.ValidarRepository.ComputedNumeroLetras(numero);

            if (string.IsNullOrEmpty(numeroLetra))
            {
                return Request.CreateResponse(HttpStatusCode.Accepted,
                    new
                    {
                        ok = false,
                        mensaje = "Numero vacio"
                    }
                );
            }

            return Request.CreateResponse(HttpStatusCode.Accepted,
                new { ok = true, numeroLetra }
            );
        }

        [HttpPut]
        [Route("editar/{id}")]
        public async Task<HttpResponseMessage> actualizarValidacion(ValidacionesRegistro model, int id)
        {
            var validacion = await _unitWork.ValidarRepository.ObtenerValidacion(id);

            if (validacion == null)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest,
                    new
                    {
                        ok = false,
                        mensaje = "El registro ingresado no se encuentra registrado"
                    }
                );
            }

            await _unitWork.ValidarRepository.Update_Validaciones(model, id);
            _unitWork.Commit();

            return Request.CreateResponse(HttpStatusCode.Accepted,
                new { ok = true, mensaje = "Se ha validado correctamente registro" }
            );

        }

        [HttpPut]
        [Route("editarestatus/{idcolaborador}/{estatus}")]
        public async Task<HttpResponseMessage> actualizarEstatus(int idcolaborador,bool estatus)
        {
            await _unitWork.ValidarRepository.Update_EstatusValidacion(estatus, idcolaborador);
            _unitWork.Commit();
            return Request.CreateResponse(HttpStatusCode.Accepted,
                new { ok = true}
            );
        }

        [HttpGet]
        [Route("reenviarcorreo/{idcolaborador}")]
        public async Task<HttpResponseMessage> reenviarValidacion(int idcolaborador)
        {

            var colaborador = await _unitWork.ColaboradorRepository.ObtenerColaborador(idcolaborador);

            if (colaborador == null)
            {
                return Request.CreateResponse(HttpStatusCode.Accepted,
                    new
                    {
                        ok = false,
                        mensaje = "No se encuentra colaborador registrado"
                    }
                );
            }


            if (!await _emailSender.SendEmailAsync(idcolaborador,colaborador.correo_electronico,colaborador.curp))
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError,
                     new
                     {
                         ok = false,
                         mensaje = "Ocurrio un error en el envio del correo",
                     });
            }

            return Request.CreateResponse(HttpStatusCode.Accepted,
                new { ok = true, mensaje="Se ha enviado el correo correctamente" }
            );
        }
    }
}
