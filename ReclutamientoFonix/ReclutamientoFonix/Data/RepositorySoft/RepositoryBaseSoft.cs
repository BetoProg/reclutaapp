﻿using System.Data;

namespace ReclutamientoFonix.Data.RepositorySoft
{
    internal abstract class RepositoryBaseSoft
    {
        protected IDbTransaction Transaction { get; private set; }
        protected IDbConnection Connection { get { return Transaction.Connection; } }

        public RepositoryBaseSoft(IDbTransaction transaction)
        {
            Transaction = transaction;
        }
    }
}
