﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ReclutamientoFonix.Data.RepositorySoft
{
    public class UnitWorkSoftland : IUnitWorkSoftland
    {
        private IDbConnection _connection;
        private IDbTransaction _transaction;
        private IPuestosRepository _puestosRepository;
        private bool _disposed;

        public UnitWorkSoftland()
        {
            _connection = new SqlConnection(Conexion.GetConnectionSoftland());
            _connection.Open();
            _transaction = _connection.BeginTransaction();
        }

        public IPuestosRepository PuestosRepository
        {
            get { return _puestosRepository ?? (_puestosRepository = new PuestosRepository(_transaction)); }
        }

        public void Commit()
        {
            try
            {
                _transaction.Commit();
            }
            catch
            {
                _transaction.Rollback();
                throw;
            }
            finally
            {
                _transaction.Dispose();
                _transaction = _connection.BeginTransaction();
                this.resetRepositories();
            }
        }

        private void resetRepositories()
        {
            _puestosRepository = null;
        }

        public void Dispose()
        {
            dispose(true);
            GC.SuppressFinalize(this);
        }

        public void dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (_transaction != null)
                    {
                        _transaction.Dispose();
                        _transaction = null;
                    }

                    if (_connection != null)
                    {
                        _connection.Dispose();
                        _connection = null;
                    }
                }
                _disposed = true;
            }
        }

        ~UnitWorkSoftland()
        {
            dispose(false);
        }
    }
}