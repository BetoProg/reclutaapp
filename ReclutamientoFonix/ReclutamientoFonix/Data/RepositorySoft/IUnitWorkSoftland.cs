﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReclutamientoFonix.Data.RepositorySoft
{
    public interface IUnitWorkSoftland:IDisposable
    {
        IPuestosRepository PuestosRepository { get; }
        void Commit();
    }
}
