﻿using ReclutamientoFonix.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ReclutamientoFonix.Data.RepositorySoft
{
    public interface IPuestosRepository
    {
        Task<IEnumerable<Puesto>> ObtenerPuestosEmpleados();
        Task<Puesto> ObtenerPuesto(string idpuesto);
    }
}
