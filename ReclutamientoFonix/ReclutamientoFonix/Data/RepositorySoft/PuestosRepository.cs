﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Dapper;
using ReclutamientoFonix.Dto;

namespace ReclutamientoFonix.Data.RepositorySoft
{
    internal class PuestosRepository : RepositoryBaseSoft, IPuestosRepository
    {

        public PuestosRepository(IDbTransaction transaction):base(transaction)
        {

        }

        public async Task<Puesto> ObtenerPuesto(string idpuesto)
        {
            var puesto = await Connection.QueryAsync<Puesto>("select IDPUESTO,DESCRIPCION,SALARIO_REFERENCIA,SALARIO_MENSUAL FROM FONIX.PUESTOS_SALARIOS with(NOLOCK) WHERE IDPUESTO=@idpuesto",
                new { idpuesto = idpuesto },transaction:Transaction);

            return puesto.FirstOrDefault();
        }

        public async Task<IEnumerable<Puesto>> ObtenerPuestosEmpleados()
        {
            return await Connection.QueryAsync<Puesto>("select distinct E.PUESTO AS IDPUESTO,P.DESCRIPCION from fonix.EMPLEADO E with(NOLOCK) LEFT JOIN fonix.PUESTO P with(NOLOCK) ON E.PUESTO = P.PUESTO WHERE E.ACTIVO = 'S' AND P.ACTIVO = 'S'"
                , transaction: Transaction);
        }
    }
}