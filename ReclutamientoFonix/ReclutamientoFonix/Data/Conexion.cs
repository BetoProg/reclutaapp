﻿using System.Configuration;

namespace ReclutamientoFonix.Data
{
    public static class Conexion
    {
        public static string GetConnection()
        {
            string connection = ConfigurationManager.ConnectionStrings["RECLUTAMIENTO"].ConnectionString;
            return connection;
        }

        public static string GetConnectionSoftland()
        {
            string connection = ConfigurationManager.ConnectionStrings["SOFTLAND"].ConnectionString;
            return connection;
        }
    }
}