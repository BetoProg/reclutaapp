﻿using ReclutamientoFonix.Dto;
using System.Threading.Tasks;

namespace ReclutamientoFonix.Data.Repository
{
    public interface IAuthRepository
    {
        Task<Usuario> GetUsuario(LoginDto model);
        Task<UsuarioReturnColaborador> GetUsuarioCurp(LoginPorCurp model);
    }
}
