﻿using ReclutamientoFonix.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReclutamientoFonix.Data.Repository
{
    public interface IValidarRepository
    {
        Task<int> InsertValidacionReg(ValidacionesRegistro validaciones);
        Task Update_Validaciones(ValidacionesRegistro validaciones,int idcolaborador);
        Task Update_EstatusValidacion(bool es_valido,int idcolaborador);
        Task<ValidacionesRegistro> ObtenerValidacion(int idcolaborador);
        Task<string> ComputedNumeroLetras(decimal numero);
    }
}
