﻿using ReclutamientoFonix.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ReclutamientoFonix.Data.Repository
{
    public interface IColaboradorRepository
    {
        Task<int> InsertColaborar(ColaboradorRegistro colaborador);
        Task InsertSalud(ColaboradorRegistro colaborador, int idcolaborador);
        Task InsertAcademico(ColaboradorRegistro colaborador, int idcolaborador);
        Task InsertFamiliares(Familiares familar, int idcolaborador);
        Task InsertExp_Laboral(Exp_Laboral explaboral, int idcolaborador);
        Task InsertReferencias(Referencias referencias, int idcolaborador);

        Task Update_Colaborador(ColaboradorRegistro colaborador, int? idcolaborador);
        Task Update_Familiares(Familiares familar, int idcolaborador, int idfam);
        Task Update_Exp_Laborales(Exp_Laboral explaboral, int idcolaborador, int idlab);
        Task Update_Referencias(Referencias referencias, int idcolaborador, int idref);
        Task Update_ColaboradorPuesto(Puesto puesto, int idcolaborador);

        Task Delete_Familiares(int idcolaborador);
        Task Delete_Exp_Laborales(int idcolaborador);
        Task Delete_Referencias(int idcolaborador);

        Task<ColaboradorRegistro> ObtenerColaborador(int idcolaborador);
        Task<IEnumerable<Reclutados>> ObtenerColaboradores();
        Task<IEnumerable<Familiares>> ObtenerFamiliares(int idcolaborador);
        Task<IEnumerable<Exp_Laboral>> ObtenerExp_Laborales(int idcolaborador);
        Task<IEnumerable<Referencias>> ObetenerReferencias(int idcolaborador);
    }
}