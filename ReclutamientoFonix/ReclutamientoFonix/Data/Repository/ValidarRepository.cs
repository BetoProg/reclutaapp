﻿using Dapper;
using ReclutamientoFonix.Dto;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace ReclutamientoFonix.Data.Repository
{
    internal class ValidarRepository : RepositoryBase, IValidarRepository
    {
        public ValidarRepository(IDbTransaction transaction) : base(transaction)
        {

        }

        public async Task<string> ComputedNumeroLetras(decimal numero)
        {

            var result = await Connection.QueryAsync<string>("SELECT RECL.NUMEROALETRAS(@numero)",new { numero=numero }, transaction: Transaction);
            return result.FirstOrDefault();
        }

        public async Task<int> InsertValidacionReg(ValidacionesRegistro validaciones)
        {
            var result = await Connection.ExecuteScalarAsync<int>("INSERT INTO Intranet.RECL.DICCIO_CAMPOS (idcolaborador ,es_valido,num_ediciones,c_nombre ,c_apellido_paterno ,c_apellido_materno ,c_edad ,c_estado_civil ,c_curp ,c_rfc ,c_seguro_social ,c_fecha_const_nopenales ,c_correo_electronico,c_tel_movil ,c_tel_casa ,c_tipo_doc ,c_folio_doc ,c_calle ,c_no_ext ,c_no_int ,c_colonia ,c_municipio ,c_cp ,c_estado ,c_nombre_contacto_emergencia ,c_tel_contacto_emergencia ,c_contacto_parentesco_emerg ,c_ult_grado_est ,c_nombre_insti ,c_doc_recibido ,c_reg_profesional ,c_a_fecha_inicio ,c_a_fecha_final ,c_familares ,c_explaborales ,usuario_validacion)"
                + " VALUES (@idcolaborador,@es_valido,@num_ediciones,@c_nombre,@c_apellido_paterno,@c_apellido_materno,@c_edad,@c_estado_civil,@c_curp,@c_rfc,@c_seguro_social,@c_fecha_const_nopenales,@c_correo_electronico,@c_tel_movil,@c_tel_casa,@c_tipo_doc,@c_folio_doc,@c_calle,@c_no_ext,@c_no_int,@c_colonia,@c_municipio,@c_cp,@c_estado,@c_nombre_contacto_emergencia,@c_tel_contacto_emergencia,@c_contacto_parentesco_emerg,@c_ult_grado_est,@c_nombre_insti,@c_doc_recibido,@c_reg_profesional,@c_a_fecha_inicio,@c_a_fecha_final,@c_familares,@c_explaborales,@usuario_validacion)",
                new
                {
                    idcolaborador = validaciones.idcolaborador,
                    es_valido= validaciones.es_valido,
                    num_ediciones = 0,
                    c_nombre = validaciones.c_nombre,
                    c_apellido_paterno = validaciones.c_apellido_paterno,
                    c_apellido_materno = validaciones.c_apellido_materno,
                    c_edad = validaciones.c_edad,
                    c_estado_civil = validaciones.c_estado_civil,
                    c_curp = validaciones.c_curp,
                    c_rfc = validaciones.c_rfc,
                    c_seguro_social = validaciones.c_seguro_social,
                    c_fecha_const_nopenales = validaciones.c_fecha_const_nopenales,
                    c_correo_electronico = validaciones.c_correo_electronico,
                    c_tel_movil = validaciones.c_tel_movil,
                    c_tel_casa = validaciones.c_tel_casa,
                    c_tipo_doc = validaciones.c_tipo_doc,
                    c_folio_doc = validaciones.c_folio_doc,
                    c_calle = validaciones.c_calle,
                    c_no_ext = validaciones.c_no_ext,
                    c_no_int = validaciones.c_no_int,
                    c_colonia = validaciones.c_colonia,
                    c_municipio = validaciones.c_municipio,
                    c_cp = validaciones.c_cp,
                    c_estado = validaciones.c_estado,
                    c_nombre_contacto_emergencia = validaciones.c_nombre_contacto_emergencia,
                    c_tel_contacto_emergencia = validaciones.c_tel_contacto_emergencia,
                    c_contacto_parentesco_emerg = validaciones.c_contacto_parentesco_emerg,
                    c_ult_grado_est = validaciones.c_ult_grado_est,
                    c_nombre_insti = validaciones.c_nombre_insti,
                    c_doc_recibido = validaciones.c_doc_recibido,
                    c_reg_profesional = validaciones.c_reg_profesional,
                    c_a_fecha_inicio = validaciones.c_a_fecha_inicio,
                    c_a_fecha_final = validaciones.c_a_fecha_final,
                    c_familares = validaciones.c_familares,
                    c_explaborales = validaciones.c_explaborales,
                    usuario_validacion = validaciones.usuario_validacion,
                }, transaction: Transaction);

            return result;
        }

        public async Task<ValidacionesRegistro> ObtenerValidacion(int idcolaborador)
        {
            var result = await Connection.QueryAsync<ValidacionesRegistro>("SELECT iddiccio ,es_valido,num_ediciones,idcolaborador ,c_nombre ,c_apellido_paterno ,c_apellido_materno ,c_edad ,c_estado_civil ,c_curp ,c_rfc ,c_seguro_social ,c_fecha_const_nopenales ,c_correo_electronico ,c_tel_movil ,c_tel_casa ,c_tipo_doc ,c_folio_doc ,c_calle ,c_no_ext ,c_no_int ,c_colonia ,c_municipio ,c_cp ,c_estado ,c_nombre_contacto_emergencia ,c_tel_contacto_emergencia ,c_contacto_parentesco_emerg ,c_ult_grado_est ,c_nombre_insti ,c_doc_recibido ,c_reg_profesional ,c_a_fecha_inicio ,c_a_fecha_final ,c_familares ,c_explaborales ,usuario_validacion FROM RECL.DICCIO_CAMPOS WHERE idcolaborador=@idcolaborador"
            , new {idcolaborador=idcolaborador }, transaction: Transaction);

            return result.FirstOrDefault();
        }

        public async Task Update_EstatusValidacion(bool es_valido,int idcolaborador)
        {
            var result = await Connection.ExecuteAsync("UPDATE RECL.DICCIO_CAMPOS SET es_valido = @es_valido WHERE idcolaborador=@idcolaborador",
                new {
                    es_valido,
                    idcolaborador
                }, transaction: Transaction);
            
        }

        public async Task Update_Validaciones(ValidacionesRegistro validaciones, int idcolaborador)
        {
            var result = await Connection.ExecuteAsync("UPDATE RECL.DICCIO_CAMPOS SET es_valido = @es_valido,num_ediciones=@num_ediciones, c_nombre = @c_nombre, c_apellido_paterno = @c_apellido_paterno, c_apellido_materno = @c_apellido_materno, c_edad = @c_edad, c_estado_civil = @c_estado_civil, c_curp = @c_curp, c_rfc = @c_rfc, c_seguro_social = @c_seguro_social, c_fecha_const_nopenales = @c_fecha_const_nopenales, c_correo_electronico = @c_correo_electronico, c_tel_movil = @c_tel_movil, c_tel_casa = @c_tel_casa, c_tipo_doc = @c_tipo_doc, c_folio_doc = @c_folio_doc, c_calle = @c_calle, c_no_ext = @c_no_ext, c_no_int = @c_no_int, c_colonia = @c_colonia, c_municipio = @c_municipio, c_cp = @c_cp, c_estado = @c_estado, c_nombre_contacto_emergencia = @c_nombre_contacto_emergencia, c_tel_contacto_emergencia = @c_tel_contacto_emergencia, c_contacto_parentesco_emerg = @c_contacto_parentesco_emerg, c_ult_grado_est = @c_ult_grado_est, c_nombre_insti = @c_nombre_insti, c_doc_recibido = @c_doc_recibido, c_reg_profesional = @c_reg_profesional, c_a_fecha_inicio = @c_a_fecha_inicio, c_a_fecha_final = @c_a_fecha_final,"
                + " c_familares = @c_familares, c_explaborales = @c_explaborales, usuario_validacion = @usuario_validacion WHERE idcolaborador=@idcolaborador",
                new {
                    idcolaborador = idcolaborador,
                    es_valido = validaciones.es_valido,
                    num_ediciones = validaciones.num_ediciones,
                    c_nombre = validaciones.c_nombre,
                    c_apellido_paterno = validaciones.c_apellido_paterno,
                    c_apellido_materno = validaciones.c_apellido_materno,
                    c_edad = validaciones.c_edad,
                    c_estado_civil = validaciones.c_estado_civil,
                    c_curp = validaciones.c_curp,
                    c_rfc = validaciones.c_rfc,
                    c_seguro_social = validaciones.c_seguro_social,
                    c_fecha_const_nopenales = validaciones.c_fecha_const_nopenales,
                    c_correo_electronico = validaciones.c_correo_electronico,
                    c_tel_movil = validaciones.c_tel_movil,
                    c_tel_casa = validaciones.c_tel_casa,
                    c_tipo_doc = validaciones.c_tipo_doc,
                    c_folio_doc = validaciones.c_folio_doc,
                    c_calle = validaciones.c_calle,
                    c_no_ext = validaciones.c_no_ext,
                    c_no_int = validaciones.c_no_int,
                    c_colonia = validaciones.c_colonia,
                    c_municipio = validaciones.c_municipio,
                    c_cp = validaciones.c_cp,
                    c_estado = validaciones.c_estado,
                    c_nombre_contacto_emergencia = validaciones.c_nombre_contacto_emergencia,
                    c_tel_contacto_emergencia = validaciones.c_tel_contacto_emergencia,
                    c_contacto_parentesco_emerg = validaciones.c_contacto_parentesco_emerg,
                    c_ult_grado_est = validaciones.c_ult_grado_est,
                    c_nombre_insti = validaciones.c_nombre_insti,
                    c_doc_recibido = validaciones.c_doc_recibido,
                    c_reg_profesional = validaciones.c_reg_profesional,
                    c_a_fecha_inicio = validaciones.c_a_fecha_inicio,
                    c_a_fecha_final = validaciones.c_a_fecha_final,
                    c_familares = validaciones.c_familares,
                    c_explaborales = validaciones.c_explaborales,
                    usuario_validacion=validaciones.usuario_validacion
                }, transaction: Transaction);
        }
    }
}