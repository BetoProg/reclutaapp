﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ReclutamientoFonix.Data.Repository
{
    public class UnitWork : IUnitWork
    {

        private IDbConnection _connection;
        private IDbTransaction _transaction;
        private IAuthRepository _authrepository;
        private IColaboradorRepository _colaboradorrepository;
        private IValidarRepository _validarrepository;
        private bool _disposed;

        public UnitWork()
        {
            _connection = new SqlConnection(Conexion.GetConnection());
            _connection.Open();
            _transaction = _connection.BeginTransaction();
        }

        public IAuthRepository AuthRepository
        {
            get { return _authrepository ?? (_authrepository = new AuthRepository(_transaction)); }
        }

        public IColaboradorRepository ColaboradorRepository
        {
            get { return _colaboradorrepository ?? (_colaboradorrepository = new ColaboradorRepository(_transaction)); }
        }

        public IValidarRepository ValidarRepository
        {
            get { return _validarrepository ?? (_validarrepository = new ValidarRepository(_transaction)); }
        }

        public void Commit()
        {
            try
            {
                _transaction.Commit();
            }
            catch
            {
                _transaction.Rollback();
                throw;
            }
            finally
            {
                _transaction.Dispose();
                _transaction = _connection.BeginTransaction();
                this.resetRepositories();
            }
        }

        private void resetRepositories()
        {
            _authrepository = null;
            _colaboradorrepository = null;
        }

        public void Dispose()
        {
            dispose(true);
            GC.SuppressFinalize(this);
        }

        public void dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (_transaction != null)
                    {
                        _transaction.Dispose();
                        _transaction = null;
                    }
                    if (_connection != null)
                    {
                        _connection.Dispose();
                        _connection = null;
                    }
                }
                _disposed = true;
            }
        }

        ~UnitWork()
        {
            dispose(false);
        }
    }
}