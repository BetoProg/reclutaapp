﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using ReclutamientoFonix.Dto;
using Dapper;
using System.Data;

namespace ReclutamientoFonix.Data.Repository
{
    internal class ColaboradorRepository :RepositoryBase, IColaboradorRepository
    {
        public ColaboradorRepository(IDbTransaction transaction):base(transaction)
        {

        }

        public async Task<int> InsertColaborar(ColaboradorRegistro colaborador)
        {
            
                var result = await Connection.ExecuteScalarAsync<int>("INSERT INTO RECL.COLABORADOR(nombre,apellido_paterno,apellido_materno,edad,estado_civil,curp,rfc,seguro_social,fecha_const_nopenales,credito_Infonavit,credito_fonacot,correo_electronico,tel_movil,tel_casa,tipo_doc,folio_doc,calle,no_ext,no_int,colonia,municipio,cp,estado,usuario_creacion,tiene_ref)"
                    + " VALUES(@nombre,@apellido_paterno,@apellido_materno,@edad,@estado_civil,@curp,@rfc,@seguro_social,@fecha_const_nopenales,@credito_Infonavit,@credito_fonacot,@correo_electronico,@tel_movil,@tel_casa,@tipo_doc,@folio_doc,@calle,@no_ext,@no_int,@colonia,@municipio,@cp,@estado,@usuario_creacion,@tiene_ref);"
                    + " SELECT CAST(SCOPE_IDENTITY() as int);",
                    new {
                        nombre =colaborador.nombre,
                        apellido_paterno = colaborador.apellido_paterno,
                        apellido_materno = colaborador.apellido_materno,
                        edad = colaborador.edad,
                        estado_civil = colaborador.estado_civil,
                        curp = colaborador.curp,
                        rfc = colaborador.rfc,
                        seguro_social = colaborador.seguro_social,
                        fecha_const_nopenales = colaborador.fecha_const_nopenales,
                        credito_Infonavit = colaborador.credito_Infonavit,
                        credito_fonacot = colaborador.credito_fonacot,
                        correo_electronico = colaborador.correo_electronico,
                        tel_movil = colaborador.tel_movil,
                        tel_casa = colaborador.tel_casa,
                        tipo_doc = colaborador.tipo_doc,
                        folio_doc = colaborador.folio_doc,
                        calle = colaborador.calle,
                        no_ext = colaborador.no_ext,
                        no_int = colaborador.no_int,
                        colonia = colaborador.colonia,
                        municipio = colaborador.municipio,
                        cp = colaborador.cp,
                        estado = colaborador.estado,
                        usuario_creacion = colaborador.usuario_creacion,
                        tiene_ref=colaborador.tiene_ref
                    },transaction:Transaction);

            return result;
        }

        public async Task InsertSalud(ColaboradorRegistro colaborador, int idcolaborador)
        {

                await Connection.ExecuteAsync("INSERT INTO RECL.SALUD VALUES(@idcolaborador,@es_alergia_med,@desc_alergia_med,@es_alergia_com,@desc_alergia_com,@enfermedad_cro,@desc_enfermedad_cro,@nombre_contacto_emergencia,@tel_contacto_emergencia,@contacto_parentesco_emerg)",
                new
                {
                    idcolaborador = idcolaborador,
                    es_alergia_med = colaborador.es_alergia_med,
                    desc_alergia_med = colaborador.desc_alergia_med,
                    es_alergia_com = colaborador.es_alergia_com,
                    desc_alergia_com = colaborador.desc_alergia_com,
                    enfermedad_cro = colaborador.enfermedad_cro,
                    desc_enfermedad_cro = colaborador.desc_enfermedad_cro,
                    nombre_contacto_emergencia = colaborador.nombre_contacto_emergencia,
                    tel_contacto_emergencia = colaborador.tel_contacto_emergencia,
                    contacto_parentesco_emerg = colaborador.contacto_parentesco_emerg
                },transaction:Transaction);
        }

        public async Task InsertAcademico(ColaboradorRegistro colaborador, int idcolaborador)
        {

                await Connection.ExecuteAsync("INSERT INTO RECL.ACADEMICO VALUES(@idcolaborador,@ult_grado_est,@nombre_lic_maestria,@nombre_insti,@doc_recibido,@reg_profesional,@fecha_inicio,@fecha_final)",
                new
                {
                    idcolaborador = idcolaborador,
                    ult_grado_est = colaborador.ult_grado_est,
                    nombre_lic_maestria = colaborador.nombre_lic_maestria,
                    nombre_insti = colaborador.nombre_insti,
                    doc_recibido = colaborador.doc_recibido,
                    reg_profesional = colaborador.reg_profesional,
                    fecha_inicio = colaborador.fecha_inicio,
                    fecha_final = colaborador.fecha_final
                },transaction:Transaction);
        }

        public async Task InsertFamiliares(Familiares familar, int idcolaborador)
        {

                await Connection.ExecuteAsync("INSERT INTO RECL.FAMILIARES VALUES(@idcolaborador,@nombre,@ape_paterno,@ape_materno,@parentesco,@fecha_nacimiento)",
                new
                {
                    idcolaborador = idcolaborador,
                    nombre = familar.nombre,
                    ape_paterno = familar.ape_paterno,
                    ape_materno = familar.ape_materno,
                    parentesco = familar.parentesco,
                    fecha_nacimiento = familar.fecha_nacimiento
                },transaction:Transaction);
        }

        public async Task InsertExp_Laboral(Exp_Laboral explaboral, int idcolaborador)
        {

                await Connection.ExecuteAsync("INSERT INTO RECL.EXP_LABORAL VALUES(@idcolaborador,@nombre_emp,@puesto,@fecha_inicio,@fecha_fin,@causa_salida,@otro_causa_sal,@nombre_jefe,@tel_contacto)",
                new
                {
                    idcolaborador = idcolaborador,
                    nombre_emp = explaboral.nombre_emp,
                    puesto = explaboral.puesto,
                    fecha_inicio = explaboral.fecha_inicio,
                    fecha_fin = explaboral.fecha_fin,
                    causa_salida = explaboral.causa_salida,
                    otro_causa_sal = explaboral.otro_causa_sal,
                    nombre_jefe = explaboral.nombre_jefe,
                    tel_contacto = explaboral.tel_contacto
                },transaction:Transaction);
        }

        public async Task InsertReferencias(Referencias referencias, int idcolaborador)
        {
            await Connection.ExecuteAsync("INSERT INTO RECL.REFERENCIAS VALUES(@idcolaborador,@nombre,@telefono_contacto)",
                new
                {
                    idcolaborador = idcolaborador,
                    nombre = referencias.nombre,
                    telefono_contacto = referencias.telefono_contacto
                }, transaction: Transaction);
        }

        public async Task Update_Colaborador(ColaboradorRegistro colaborador, int? idcolaborador)
        {
            await Connection.ExecuteAsync("UPDATE RECL.COLABORADOR SET nombre=@nombre,apellido_paterno=@apellido_paterno,apellido_materno=@apellido_materno,edad=@edad,estado_civil=@estado_civil,curp=@curp,rfc=@rfc,seguro_social=@seguro_social,"
            + "fecha_const_nopenales = @fecha_const_nopenales, credito_Infonavit =@credito_Infonavit, credito_fonacot = @credito_fonacot, correo_electronico = @correo_electronico, tel_movil = @tel_movil, tel_casa = @tel_casa, tipo_doc = @tipo_doc,"
            + "folio_doc = @folio_doc, calle = @calle, no_ext = @no_ext, no_int =@no_int , colonia = @colonia, municipio = @municipio, cp = @cp, estado = @estado, tiene_ref=@tiene_ref,idpuesto=@idpuesto,descripcion_puesto=@descripcion_puesto,salario_referencia=@salario_referencia,salario_mensual=@salario_mensual,html_mapa=@html_mapa"
            + " where idcolaborador = @idcolaborador", new
            {
                idcolaborador = idcolaborador,
                nombre = colaborador.nombre,
                apellido_paterno = colaborador.apellido_paterno,
                apellido_materno = colaborador.apellido_materno,
                edad = colaborador.edad,
                estado_civil = colaborador.estado_civil,
                curp = colaborador.curp,
                rfc = colaborador.rfc,
                seguro_social = colaborador.seguro_social,
                fecha_const_nopenales = colaborador.fecha_const_nopenales,
                credito_Infonavit = colaborador.credito_Infonavit,
                credito_fonacot = colaborador.credito_fonacot,
                correo_electronico = colaborador.correo_electronico,
                tel_movil = colaborador.tel_movil,
                tel_casa = colaborador.tel_casa,
                tipo_doc = colaborador.tipo_doc,
                folio_doc = colaborador.folio_doc,
                calle = colaborador.calle,
                no_ext = colaborador.no_ext,
                no_int = colaborador.no_int,
                colonia = colaborador.colonia,
                municipio = colaborador.municipio,
                cp = colaborador.cp,
                estado = colaborador.estado,
                tiene_ref= colaborador.tiene_ref,
                idpuesto = colaborador.idpuesto,
                descripcion_puesto = colaborador.descripcion_puesto,
                salario_referencia = colaborador.salario_referencia,
                salario_mensual = colaborador.salario_mensual,
                html_mapa = colaborador.html_mapa
            }, transaction: Transaction);

            await Connection.ExecuteAsync("update RECL.ACADEMICO set ult_grado_est=@ult_grado_est,nombre_lic_maestria=@nombre_lic_maestria,nombre_insti=@nombre_insti,doc_recibido=@doc_recibido,reg_profesional=@reg_profesional,fecha_inicio=@fecha_inicio,fecha_final=@fecha_final"
            + " where idcolaborador = @idcolaborador", new
            {
                idcolaborador = idcolaborador,
                ult_grado_est = colaborador.ult_grado_est,
                nombre_lic_maestria = colaborador.nombre_lic_maestria,
                nombre_insti = colaborador.nombre_insti,
                doc_recibido = colaborador.doc_recibido,
                reg_profesional = colaborador.reg_profesional,
                fecha_inicio = colaborador.fecha_inicio,
                fecha_final = colaborador.fecha_final
            }, transaction: Transaction);

            await Connection.ExecuteAsync("update RECL.SALUD set es_alergia_med=@es_alergia_med,desc_alergia_med=@desc_alergia_med,es_alergia_com=@es_alergia_com,desc_alergia_com=@desc_alergia_com,enfermedad_cro=@enfermedad_cro,desc_enfermedad_cro=@desc_enfermedad_cro,"
            + "nombre_contacto_emergencia = @nombre_contacto_emergencia, tel_contacto_emergencia = @tel_contacto_emergencia, contacto_parentesco_emerg = @contacto_parentesco_emerg"
            + " where idcolaborador = @idcolaborador", new
            {
                idcolaborador = idcolaborador,
                es_alergia_med = colaborador.es_alergia_med,
                desc_alergia_med = colaborador.desc_alergia_med,
                es_alergia_com = colaborador.es_alergia_com,
                desc_alergia_com = colaborador.desc_alergia_com,
                enfermedad_cro = colaborador.enfermedad_cro,
                desc_enfermedad_cro = colaborador.desc_enfermedad_cro,
                nombre_contacto_emergencia = colaborador.nombre_contacto_emergencia,
                tel_contacto_emergencia = colaborador.tel_contacto_emergencia,
                contacto_parentesco_emerg = colaborador.contacto_parentesco_emerg
            }, transaction: Transaction);
        }

        public async Task Update_Familiares(Familiares familar, int idcolaborador, int idfam)
        {
            await Connection.ExecuteAsync("update RECL.FAMILIARES set nombre=@nombre,ape_paterno=@ape_paterno,ape_materno=@ape_materno,parentesco=@parentesco,fecha_nacimiento=@fecha_nacimiento"
            + " where idcolaborador = @idcolaborador and idfam = @idfam", new
            {
                idcolaborador = idcolaborador,
                idfam = idfam,
                nombre = familar.nombre,
                ape_paterno = familar.ape_paterno,
                ape_materno = familar.ape_materno,
                parentesco = familar.parentesco,
                fecha_nacimiento = familar.fecha_nacimiento
            }, transaction: Transaction);
        }

        public async Task Update_Exp_Laborales(Exp_Laboral explaboral, int idcolaborador, int idlab)
        {
            await Connection.ExecuteAsync("update RECL.EXP_LABORAL set nombre_emp=@nombre_emp,puesto=@puesto,fecha_inicio=@fecha_inicio,fecha_fin=@fecha_fin,causa_salida=@causa_salida,nombre_jefe=@nombre_jefe,tel_contacto=@nombre_jefe"
            + " where idcolaborador = @idcolaborador and idlab = @idlab", new
            {
                idcolaborador = idcolaborador,
                idlab = idlab,
                nombre_emp = explaboral.nombre_emp,
                puesto = explaboral.puesto,
                fecha_inicio = explaboral.fecha_inicio,
                fecha_fin = explaboral.fecha_fin,
                causa_salida = explaboral.causa_salida,
                otro_causa_sal = explaboral.otro_causa_sal,
                nombre_jefe = explaboral.nombre_jefe,
                tel_contacto = explaboral.tel_contacto
            }, transaction: Transaction);
        }

        public async Task Update_Referencias(Referencias referencias, int idcolaborador, int idref)
        {
            await Connection.ExecuteAsync("update RECL.REFERENCIAS set nombre=@nombre,telefono_contacto=@telefono_contacto"
            + " where idcolaborador = @idcolaborador and idref = @idref", new
            {
                idcolaborador = idcolaborador,
                idref = idref,
                nombre = referencias.nombre,
                telefono_contacto = referencias.telefono_contacto
            }, transaction: Transaction);
        }

        public async Task Update_ColaboradorPuesto(Puesto puesto, int idcolaborador)
        {
            await Connection.ExecuteAsync("update RECL.COLABORADOR set idpuesto=@idpuesto,descripcion_puesto=@descripcion_puesto"
           + " where idcolaborador = @idcolaborador", new
           {
               idcolaborador = idcolaborador,
               idpuesto= puesto.idpuesto,
               descripcion_puesto= puesto.descripcion,
           }, transaction: Transaction);
        }

        public async Task Delete_Familiares(int idcolaborador)
        {
            await Connection.ExecuteAsync("delete from RECL.FAMILIARES"
            + " where idcolaborador = @idcolaborador", new
            {
                idcolaborador = idcolaborador
            }, transaction: Transaction);
        }

        public async Task Delete_Exp_Laborales(int idcolaborador)
        {
            await Connection.ExecuteAsync("delete from RECL.EXP_LABORAL"
            + " where idcolaborador = @idcolaborador", new
            {
                idcolaborador = idcolaborador
            }, transaction: Transaction);
        }

        public async Task Delete_Referencias(int idcolaborador)
        {
            await Connection.ExecuteAsync("delete from RECL.REFERENCIAS"
          + " where idcolaborador = @idcolaborador", new
          {
              idcolaborador = idcolaborador
          }, transaction: Transaction);
        }

        public async Task<ColaboradorRegistro> ObtenerColaborador(int idcolaborador)
        {
            var colaborador = await Connection.QueryAsync<ColaboradorRegistro>("select c.idcolaborador, nombre, apellido_paterno, apellido_materno, edad,estado_civil, curp, rfc, seguro_social, fecha_const_nopenales, credito_Infonavit, credito_fonacot, correo_electronico, tel_movil, tel_casa, tipo_doc, folio_doc, calle, no_ext, no_int, colonia, municipio, cp, estado, usuario_creacion, es_alergia_med, desc_alergia_med, es_alergia_com, desc_alergia_com, enfermedad_cro, desc_enfermedad_cro, nombre_contacto_emergencia, tel_contacto_emergencia, contacto_parentesco_emerg, ult_grado_est, nombre_lic_maestria, nombre_insti, doc_recibido, reg_profesional, fecha_inicio, fecha_final,tiene_ref,idpuesto,descripcion_puesto,salario_referencia,salario_mensual,html_mapa, createdate as fecha_creacion from RECL.COLABORADOR C INNER JOIN RECL.ACADEMICO A on C.idcolaborador = a.idcolaborador INNER JOIN RECL.SALUD S on C.idcolaborador = S.idcolaborador where A.idcolaborador = @idcolaborador",
                new { idcolaborador = idcolaborador }, transaction: Transaction);

            return colaborador.FirstOrDefault();
        }

        public async Task<IEnumerable<Familiares>> ObtenerFamiliares(int idcolaborador)
        {
            return await Connection.QueryAsync<Familiares>("select idcolaborador, nombre, ape_paterno, ape_materno, parentesco, fecha_nacimiento from RECL.FAMILIARES WHERE idcolaborador=@idcolaborador",
                new { idcolaborador = idcolaborador }, transaction: Transaction);
        }

        public async Task<IEnumerable<Exp_Laboral>> ObtenerExp_Laborales(int idcolaborador)
        {
            return await Connection.QueryAsync<Exp_Laboral>("select nombre_emp,puesto,fecha_inicio,fecha_fin,causa_salida,otro_causa_sal,nombre_jefe,tel_contacto from RECL.EXP_LABORAL WHERE idcolaborador=@idcolaborador",
                new { idcolaborador = idcolaborador }, transaction: Transaction);
        }

        public async Task<IEnumerable<Reclutados>> ObtenerColaboradores()
        {
            return await Connection.QueryAsync<Reclutados>("select c.idcolaborador, c.nombre,c.apellido_paterno, c.apellido_materno,c.correo_electronico, c.html_mapa,c.createdate as fecha_creacion  from RECL.COLABORADOR C INNER JOIN RECL.ACADEMICO A on C.idcolaborador = a.idcolaborador INNER JOIN RECL.SALUD S on C.idcolaborador = S.idcolaborador order by c.createdate desc"
             , transaction: Transaction);
        }

        public async Task<IEnumerable<Referencias>> ObetenerReferencias(int idcolaborador)
        {
            return await Connection.QueryAsync<Referencias>("select nombre,telefono_contacto from RECL.REFERENCIAS WHERE idcolaborador=@idcolaborador",
            new { idcolaborador = idcolaborador }, transaction: Transaction);
        }

        
    }

      
    
}