﻿using Dapper;
using ReclutamientoFonix.Dto;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace ReclutamientoFonix.Data.Repository
{
    internal class AuthRepository : RepositoryBase, IAuthRepository
    {
        public AuthRepository(IDbTransaction transaction):base(transaction)
        {

        }

        public async Task<Usuario> GetUsuario(LoginDto model)
        {
            var usuarioEncontrado = await Connection.QueryAsync<Usuario>("SELECT USERNAME,PERFIL FROM RECL.USUARIO WHERE USERNAME=@username and PASSWORD=@password",
            new { username=model.username, password=model.password },transaction:Transaction);

            return usuarioEncontrado.FirstOrDefault();
        }

        public async Task<UsuarioReturnColaborador> GetUsuarioCurp(LoginPorCurp model)
        {
            var registroEncontrado = await Connection.QueryAsync<UsuarioReturnColaborador>("select idcolaborador, curp from RECL.COLABORADOR where curp=@curp",
                new { curp=model.curp }, transaction: Transaction);

            return registroEncontrado.FirstOrDefault();
        }
    }
}