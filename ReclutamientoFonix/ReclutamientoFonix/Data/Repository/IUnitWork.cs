﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReclutamientoFonix.Data.Repository
{
    public interface IUnitWork:IDisposable
    {
        IAuthRepository AuthRepository { get; }
        IColaboradorRepository ColaboradorRepository { get; }
        IValidarRepository ValidarRepository { get; }
        void Commit();
    }
}
