﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReclutamientoFonix.Dto
{
    public class Puesto
    {
        public string idpuesto { get; set; }
        public string descripcion { get; set; }
        public decimal salario_referencia { get; set; }
        public decimal salario_mensual { get; set; }
    }
}