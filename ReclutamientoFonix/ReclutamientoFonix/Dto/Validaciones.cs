﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReclutamientoFonix.Dto
{
    public class ValidacionesRegistro
    {
        public int iddiccio { get; set; }
        public int idcolaborador { get; set; }

        public bool es_valido { get; set; }
        public int num_ediciones { get; set; }
        public bool c_nombre { get; set; }
        public bool c_apellido_paterno { get; set; }
        public bool c_apellido_materno { get; set; }
        public bool c_edad { get; set; }
        public bool c_estado_civil { get; set; }
        public bool c_curp { get; set; }
        public bool c_rfc { get; set; }
        public bool c_seguro_social { get; set; }
        public bool c_fecha_const_nopenales { get; set; }
        public bool c_correo_electronico { get; set; }
        public bool c_tel_movil { get; set; }
        public bool c_tel_casa { get; set; }
        public bool c_tipo_doc { get; set; }
        public bool c_folio_doc { get; set; }
        public bool c_calle { get; set; }
        public bool c_no_ext { get; set; }
        public bool c_no_int { get; set; }
        public bool c_colonia { get; set; }
        public bool c_municipio { get; set; }
        public bool c_cp { get; set; }
        public bool c_estado { get; set; }
        public bool c_nombre_contacto_emergencia { get; set; }
        public bool c_tel_contacto_emergencia { get; set; }
        public bool c_contacto_parentesco_emerg { get; set; }
        public bool c_ult_grado_est { get; set; }
        public bool c_nombre_insti { get; set; }
        public bool c_doc_recibido { get; set; }
        public bool c_reg_profesional { get; set; }
        public bool c_a_fecha_inicio { get; set; }
        public bool c_a_fecha_final { get; set; }
        public bool c_familares { get; set; }
        public bool c_explaborales { get; set; }

        public string usuario_validacion { get; set; }
    }
}