﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ReclutamientoFonix.Dto
{
    public class LoginDto
    {
        [Required]
        public string username { get; set; }
        [Required]
        public string password { get; set; }
    }

    public class LoginPorCurp
    {
        [Required]
        public string curp { get; set; }
    }

    public class UsuarioReturnColaborador
    {
        public int idcolaborador { get; set; }
        public string curp { get; set; }
    }


    public class UsuarioReturnDto
    {
        public bool ok { get; set; }
        public Usuario usuario { get; set; }
        public string token { get; set; }
    }

    public class Usuario
    {
        public string username { get; set; }
        public string perfil { get; set; }
    }
}