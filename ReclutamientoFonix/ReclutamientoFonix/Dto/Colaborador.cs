﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ReclutamientoFonix.Dto
{
    public class ColaboradorRegistro
    {
        private string pnombre;
        private string ape_ppaterno;
        private string ape_pmaterno;
        private string pcurp;
        private string prfc;

        public int? idcolaborador { get; set; }
        //CAMPOS INICIALES
        [StringLength(150)]
        public string nombre {
            get { return pnombre; }
            set { pnombre = value.ToUpper(); }
        }

        [StringLength(120)]
        public string apellido_paterno {
            get { return ape_ppaterno; }
            set { ape_ppaterno = value.ToUpper(); }
        }

        [StringLength(120)]
        public string apellido_materno {
            get { return ape_pmaterno; }
            set { ape_pmaterno = value.ToUpper(); }
        }

        public int edad { get; set; }
        [StringLength(12)]
        public string estado_civil { get; set; }

        [StringLength(18)]
        public string curp {
            get { return pcurp; }
            set { pcurp = value.ToUpper(); }
        }

        [StringLength(13)]
        public string rfc {
            get { return prfc; }
            set { prfc = value.ToUpper(); }
        }

        [StringLength(11)]
        public string seguro_social { get; set; }


        public DateTime fecha_const_nopenales { get; set; }

        public bool credito_Infonavit { get; set; }
        public bool credito_fonacot { get; set; }

        [StringLength(120)]
        public string correo_electronico { get; set; }

        [StringLength(10)]
        public string tel_movil { get; set; }

        [StringLength(10)]
        public string tel_casa { get; set; }

        [StringLength(50)]
        public string tipo_doc { get; set; }

        [StringLength(50)]
        public string folio_doc { get; set; }

        [StringLength(80)]
        public string calle { get; set; }

        [StringLength(15)]
        public string no_ext { get; set; }

        [StringLength(15)]
        public string no_int { get; set; }

        [StringLength(50)]
        public string colonia { get; set; }

        [StringLength(50)]
        public string municipio { get; set; }

        [StringLength(10)]
        public string cp { get; set; }

        [StringLength(30)]
        public string estado { get; set; }

        [StringLength(30)]
        public string usuario_creacion { get; set; }

        //CAMPOS DE SALUD
        public bool es_alergia_med { get; set; }

        [StringLength(120)]
        public string desc_alergia_med { get; set; }
        public bool es_alergia_com { get; set; }

        [StringLength(120)]
        public string desc_alergia_com { get; set; }
        public bool enfermedad_cro { get; set; }

        [StringLength(120)]
        public string desc_enfermedad_cro { get; set; }

        [StringLength(120)]
        public string nombre_contacto_emergencia { get; set; }

        [StringLength(10)]
        public string tel_contacto_emergencia { get; set; }

        [StringLength(180)]
        public string contacto_parentesco_emerg { get; set; }

        //ACADEMICO
        [StringLength(50)]
        public string ult_grado_est { get; set; }

        public string nombre_lic_maestria { get; set; }

        [StringLength(120)]
        public string nombre_insti { get; set; }

        [StringLength(50)]
        public string doc_recibido { get; set; }

        [StringLength(80)]
        public string reg_profesional { get; set; }

        public DateTime fecha_inicio { get; set; }
        public DateTime fecha_final { get; set; }

        public bool tiene_ref { get; set; }

        public string idpuesto { get; set; }
        public string descripcion_puesto { get; set; }
        public decimal salario_referencia { get; set; }
        public decimal salario_mensual { get; set; }

        public string html_mapa { get; set; }
        public DateTime fecha_creacion { get; set; }

        public ICollection<Familiares> Familiares { get; set; }
        public ICollection<Exp_Laboral> Exp_Laborales { get; set; }
        public ICollection<Referencias> Referencias { get; set; }
    }

    public class Familiares
    {
        public int idfam { get; set; }

        public int idcolaborador { get; set; }

        [StringLength(120)]
        public string nombre { get; set; }

        [StringLength(120)]
        public string ape_paterno { get; set; }

        [StringLength(120)]
        public string ape_materno { get; set; }

        [StringLength(50)]
        public string parentesco { get; set; }

        public DateTime fecha_nacimiento { get; set; }

        public virtual ColaboradorRegistro Colaborador { get; set; }
    }

    public class Exp_Laboral
    {
        public int idlab { get; set; }

        public int idcolaborado { get; set; }

        [StringLength(120)]
        public string nombre_emp { get; set; }

        [StringLength(50)]
        public string puesto { get; set; }
        public DateTime fecha_inicio { get; set; }
        public DateTime fecha_fin { get; set; }

        [StringLength(50)]
        public string causa_salida { get; set; }

        [StringLength(50)]
        public string otro_causa_sal { get; set; }

        [StringLength(50)]
        public string nombre_jefe { get; set; }

        [StringLength(10)]
        public string tel_contacto { get; set; }

        public virtual ColaboradorRegistro Colaborador { get; set; }
    }

    public class Referencias
    {
        public int idref { get; set; }
        public int idcolaborador { get; set; }
        [StringLength(180)]
        public string nombre { get; set; }
        [StringLength(10)]
        public string telefono_contacto { get; set; }
    }

    public class Reclutados
    {
        public int idcolaborador { get; set; }
        public bool es_valido { get; set; }
        public int num_ediciones { get; set; }
        public string nombre { get; set; }
        public string apellido_paterno { get; set; }
        public string apellido_materno { get; set; }
        public string correo_electronico { get; set; }

        public bool tieneValidacion { get; set; }
        public string html_mapa { get; set; }
        public DateTime fecha_creacion { get; set; }
    }

    public class ColaboradorActualizar
    {
        public ColaboradorRegistro colaborador { get; set; }
        public ValidacionesRegistro validaciones { get; set; }
    }
}