﻿using System;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace ReclutamientoFonix.Services
{
    public class EmailSender : IEmailSender
    {
        private string body { get; set; }
        private string contenido { get; set; }
        private string subject { get; set; }
        private string url { get; set; }
        private string fromEmail = "activaciones.fonixsi@fonix.com.mx";
        public string password = "Fonix2018123";

        public async Task<bool> SendEmailAsync(int id ,string addemail,string curp)
        {
            bool isSend = false;
            try
            {
                // Inicializacion.  
                var message = new MailMessage();
                subject = "Solicitud de Reclutamiento";
                message.To.Add(new MailAddress("juan.bonilla@fonix.com.mx"));
                //message.To.Add(new MailAddress("francisco.ruiz@fonix.com.mx"));
                url = "http://intranett.fonix.com.mx/reclutamientofonix/page/login";
                contenido = $"<h1>Solicitud de Reclutamiento No {id}</h1>";

                if (!string.IsNullOrEmpty(addemail))
                {
                    subject = "Correccion de datos";
                    url = "http://intranett.fonix.com.mx/reclutamientofonix/page/login/"+curp;
                    contenido = "<h1>En el siguiente hipervinculo se especifica los datos a corregir</h1>";
                    message.To.Add(new MailAddress(addemail));
                }

                body = builderBody(id, url,contenido);
                message.From = new MailAddress("activaciones.fonixsi@fonix.com.mx");
                message.Subject = subject;
                message.Body = body;
                message.IsBodyHtml = true;

                using (var smtp = new SmtpClient())
                {
                    // Config.  
                    var credential = new NetworkCredential
                    {
                        UserName = fromEmail,//EmailInfo.FROM_EMAIL_ACCOUNT,
                        Password = password//EmailInfo.FROM_EMAIL_PASSWORD
                    };

                    // Config.  
                    smtp.Credentials = credential;
                    smtp.Host = "smtp.office365.com";//EmailInfo.SMTP_HOST_GMAIL;
                    smtp.Port = 587;//EmailInfo.SMTP_PORT_GMAIL);
                    smtp.EnableSsl = true;

                    // Config  
                    await smtp.SendMailAsync(message);

                    // Settings.  
                    isSend = true;
                }
            }
            catch (Exception ex)
            { 
                return false;
                throw ex;
            }

            return isSend;
        }

        private string builderBody(int id,string url=null, string contenido=null)
        {
            var body = new StringBuilder();
            body.AppendLine("<body style='font-family:Arial;'>");
            body.AppendLine(contenido);
            body.AppendLine($"<a href='{url}'>Validar Solicitud</a>");
            body.AppendLine("</body>");
            return body.ToString();
        }
    }
}