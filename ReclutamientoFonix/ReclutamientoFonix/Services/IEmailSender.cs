﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace ReclutamientoFonix.Services
{
    public interface IEmailSender
    {
        Task<bool> SendEmailAsync(int id,string addemail=null,string curp=null);

        
    }
}
